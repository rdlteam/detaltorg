<?php
namespace Rdl\SecurityFirewall\Exception;

class ErrorPage extends \Exception {
	public function Display(){
		
		$arConfig = require_once(ENGINE_DIR . 'Core/config/global.config.php');
		$Path = ENGINE_DIR . 'templates/' . $arConfig['Site']['Template'] .'/access_denied.phtml';
		if(file_exists($Path)){
			require_once $Path;
			die();
		}
		
		echo '<html>
			  <head>
			  <title>Hacking attempt</title>
			  </head>
			  <body>
			  	<center><strong>Hacking attempt!</strong></center>
			  </body>
			  </html>';
	}
}