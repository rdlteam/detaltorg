<?php
/*
* RDL CMS created by RDL Team
* http://rdl-team.ru
* Copyright (c) 2009-2014 RDL Team
* Данный код защищен авторскими правами
*
* Файл: Route.php
* Назначение: Класс для маршрутизации
*/
namespace Rdl\Route;

use Zend\Cache\StorageFactory;
use \Rdl\Request\Request;

class Route extends \Rdl\CoreFactory {

	private $Cache = false;

	public $arPages = array();
	public $Page = false;
	public $RealPage = false;
	public $Params  = array();
	public $RouteRule = array();
	public $indexPages = array('index.php', 'index.html');

	function __construct(){
		parent::__construct();

		$this->RouteRule = $this->Core->Service == 'index' ? include_once(ENGINE_DIR . 'RouteRule.php') : array();
	}

	public function Run(){
		if($this->Core->Service != 'index' AND $this->Core->Service != 'admin') return false;

		$parseUrl 		= parse_url(rawurldecode($_SERVER['REQUEST_URI']));
		if(mb_strlen($parseUrl['path']) > 1 && strpos($parseUrl['path'], '.') === false){
			$slesh = preg_replace("/\?.*/i",'', $parseUrl['path']);
			if(rtrim($slesh, '/')."/" != $slesh){
				header("HTTP/1.1 301 Moved Permanently");
				header('Location: '.$this->Core->Url . $slesh .'/');
				die();
			}
		}

		$this->RealPage = &$parseUrl['path'];

		if($this->Core->Service == 'admin')
			$this->RealPage = str_replace($this->Core->Config->admin . '/', '', $this->RealPage);
			// die($this->RealPage);

		if(!empty($_GET))	
			$this->Params = array_merge($this->Params, $_GET);

			$this->Page = sha1($this->RealPage);
			
		if(count($this->RouteRule) > 0){
			foreach($this->RouteRule as $Mask => $Link){
				if(preg_match($Mask, $this->RealPage, $str)){
					if(isset($Link['params']))	foreach($Link['params'] as $name => $id){
						$this->Params[$name] = isset($str[$id]) ? $str[$id] : false;
					}
					$this->RealPage = $Link['page'];
					$this->Page 	= sha1($this->RealPage);
					$this->Params['Action'] = isset($Link['Action']) ? $Link['Action'] : 'index';
					break;
				}
			}
		}

		// die($this->RealPage);
	}

	public function getParams($page) {

	}

	/*
	* Метод для ответа, выдает требуемый ответ в случае верной страницы.
	* Первый параметр обязателен
	*/
	public function thisPage($page = '', $return = ''){
		if(empty($page))
			return false;

		if(strpos($page, '.') === false)
			$page = rtrim($page, '/') . '/';

		$inArray[] = $page;
		if(count($this->indexPages) > 0) foreach($this->indexPages as $Page){
			$inArray[] = $page . $Page;
		}

		$Return = in_array($this->RealPage, array_values($inArray));
		
		unset($inArray);
		if(!empty($return))
			$Return = $Return ? $return : false;

		return $Return;
	}

	public function getAjax(){
		return;
	}
}