<?php
namespace Rdl\Microtimer;

class Microtimer {
	public $time;
	private static $instance = false;

	static function getInstance(){
		if(self::$instance === false)
			self::$instance = new self;

		return self::$instance;
	}

	public function __construct(){}

	public function start(){
		$this->time = $this->get_real_time();
	}

	public function get() {
		return round( ($this->get_real_time() - $this->time), 5 );
	}

	public function get_real_time() {
		list ( $seconds, $microSeconds ) = explode( ' ', microtime() );
		return (( float ) $seconds + ( float ) $microSeconds);
	}
}