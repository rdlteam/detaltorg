<?php
namespace Rdl\Exception;
use Zend\Http\Response;

class ErrorPage extends ExceptionFactory {
	public function Display(){
		$this->genereateHeader();
		// В случае нахождения шаблона пользователя
		if(file_exists($this->Registry->Template->_path .'404.phtml')){
			$this->Registry->Template->Display(404);
			return true;
		} 

		$obTemplate = new \Rdl\View\Template('global');
		$obTemplate->Set('Header', $this->Registry->Helper->headMeta() . "\n");
		$obTemplate->Set('Title',  $this->Registry->Helper->headTitle() . "\n");
		$obTemplate->Display(404);
		$obTemplate->Clear();
	}
}