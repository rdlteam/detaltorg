<?php
/*
* RDL CMS created by RDL Team
* http://rdl-team.ru
* Copyright (c) 2009-2014 RDL Team
* Данный код защищен авторскими правами
*
* Файл: Request.php
* Назначение: Класс для работы со входящими данными
*/

namespace Rdl\Request;
Class Request extends \Rdl\CoreFactory{
    /**
    * Публичный метод обертка над статичным методом GetParam, для быстрого доступа к глобальной переменной $_GET
    * @author LeMaX (rdl-team.ru)
    * @return array | bool | object | string
    * @static
    */
    public static function Get($var = '', $save = true, $default_value = false){
        return self::GetParam(array('var' => $var, 'save' => $save, 'method' => 'GET', 'default' => $default_value));
    }

    /**
    * Публичный метод обертка над статичным методом GetParam, для быстрого доступа к глобальной переменной $_POST
    * @author LeMaX (rdl-team.ru)
    * @return array | bool | object | string
    * @static
    */
    public static function Post($var = '', $save = true, $default_value = false){
        return self::GetParam(array('var' => $var, 'save' => $save, 'method' => 'POST', 'default' => $default_value));
    }
    
    /**
    * Статичный метод GetParam для доступа к видам глобальных переменных POST, GET, REQUEST, SERVER, COOKIE с возможностью мгновенной фильтрации данных
    * @author LeMaX (rdl-team.ru)
    * @return array | bool | object | string
    * @static
    */
    public static function GetParam(array $param = array()){
        if(!is_array($param))
            return false;

        if(empty($param['method']) OR !$param['method'])
            $param['method'] = 'GET';

        else if(!in_array($param['method'], array('GET', 'POST', 'REQUEST', 'SERVER', 'COOKIE')))
            return false;

        if(empty($param['var']) && !empty($param['method']))
            $param['var'] = self::method(false, $param['method']);
        
        if(empty($param['save']))
            $param['save'] = false;


        if(is_array($param['var']) && count($param['var']) > 0){

            $return = array();

            foreach($var as $key => $value){
                //Проверяем, глобальные настройки, или же персональные для каждой переменной
                if(is_array($param['save']) && isset($param['save'][$value])) 
                    $save = $param['save'][$value];

                if(!($return[$value] = self::GetParam(
                                            array(
                                                'var' => $value, 
                                                'save' => $param['save']
                                            )
                                        ))){
                    return false;
                }
            }

            return $return;

        } else {
            
            if(!self::method($param['method'], $param['var'])) 
                return !isset($param['default']) ? false : $param['default'];

            return $param['save'] == true ? self::Filter(self::method($param['method'], $param['var']), $param['save']) : self::method($param['method'], $param['var']);

        }
    }

    /**
    * Статичный метод для доступа к не фильтруемым данным + проверка на существование ключа
    * @author LeMaX (rdl-team.ru)
    * @return array | bool | object | string
    * @static 
    */
    public static function method($method = 'GET', $var = false){
        switch(mb_strtoupper($method)){
            case 'GET':
                if(empty($var) OR !$var)
                    return $_GET;

                return isset($_GET[$var]) ? $_GET[$var] : false;
                break;
            case 'POST':
                 if(empty($var) OR !$var)
                    return $_POST;

                return isset($_POST[$var]) ? $_POST[$var] : false;
                break;
        }
    }

    /**
    * Статичный метод обертка над функцией filter_var, используется для фильтрации входящих данных по шаблону
    * @author LeMaX (rdl-team.ru)
    * @return bool | string
    * @static
    */
    public static function Filter($value, $type = 'default'){
        if(empty($value))
            return false;

        $return = array();

        switch(mb_strtolower($type)){
            case "int":
                $return['filter'] = FILTER_SANITIZE_NUMBER_INT;
            break;
            case "intval":
                $return['filter'] = FILTER_VALIDATE_INT;
            break;
            case "float":
                $return['filter'] = FILTER_SANITIZE_NUMBER_FLOAT;
            break;
            case "floatval":
                $return['filter'] = FILTER_VALIDATE_FLOAT;
            break;
            default:
               $return['filter'] = array('filter' => FILTER_SANITIZE_STRING);
            break;

        }

        if(is_array($return['filter']))
            return filter_var($value, $return['filter']['filter']);
        
        return filter_var($value, $return['filter']);
    }

    /**
    * Статичный метод для индентификации POST запроса
    * @author LeMaX (rdl-team.ru)
    * @return bool
    * @static
    */
    public static function isPost(){
        return !empty($_POST) ? true : false;
    }

    /**
    * Метод является оберткой над функцией заголовков(Header), в случае не возможности отсылки заголовка, метод завершит себя.
    * @author LeMaX (rdl-team.ru)
    * @return void | bool
    * @static
    */
    public static function Header($value){
        if(!headers_sent())
            header($value);
        else
            return false;
    }
}
?>