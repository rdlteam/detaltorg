<?php

/*
	Абстрактный класс, наследуется всеми компонентами библиотеки Rdl.
	Хранит в себе синглотон вызов ядра и хранилища классов
*/

namespace Rdl;

use Zend\Cache\StorageFactory;

abstract class CoreFactory{
	
	protected $Core;
	protected $Registry;
	protected $obCache;

	function __construct(){
		$this->Core = \RDLCore::getInstance();
		$this->Registry = \Registry::getInstance();

		$this->obCache = StorageFactory::factory(
			array(
				'adapter' => array(
					'name' => 'filesystem',
					'options' => array('cache_dir' => ENGINE_DIR .'cache/'),
				),
			)
		);
	}
	
	public function ClearCache($cache){
		return $this->obCache->removeItem($cache);
	}
}