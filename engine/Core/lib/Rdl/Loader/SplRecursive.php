<?php
namespace Rdl\Loader;

class SplRecursive extends \FilterIterator{

  	protected $ext = array('rule');

  	public function __construct($path){
  		parent::__construct(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path)));
  	}

  	public function accept(){
  		$item = $this->getInnerIterator();

  		if($item->isFile() && in_array(pathinfo($item->getFilename(), PATHINFO_EXTENSION), $this->ext)) {
  			return TRUE;
  		}

	}
}