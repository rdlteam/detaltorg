<?php
if(!Defined('ROOT'))
    die('Access denied!');

class Cache extends Cache_driver {
    private $memcache,
            $timeout = 3600;
    public $count = 0;#Кол-во файлов с данными вызванными из кеша

    function __construct(Core $core){
        $this->Core = $core;
        $this->memcache = memcache_connect(MEMCACHE_HOST, MEMCACHE_PORT);
    }

    /**
     * Получение кеша из memcache
     * @access public
     * @since 0.2
     * @param string $name
     * @param int $timeout
     * @return string\bool
     */
    public function Get($name, $timeout = 3600){
        if(DEBUG)
            return false;
        $this->timeout = $timeout;
        if($this->Core->Modules->Get('Multilang'))
            $name .= '_'.$this->Core->Multilang->Get();
        if(($cache=memcache_get($this->memcache, $name))){
            $this->count++;
            return $cache;
        }else
            return false;
    }

    /**
     * Установка кеша в memcache
     * @access public
     * @since 0.2
     * @param string $name
     * @param string $value
     * @return bool
     */
    public function Set($name, $value){
        if(DEBUG)
            return false;
        if($this->Core->Modules->Get('Multilang'))
            $name .= '_'.$this->Core->Multilang->Get();
        return memcache_set($this->memcache, $name, $value, MEMCACHE_COMPRESSED, $this->timeout);
    }

    /**
     * Очистка кеша memcache
     * @access public
     * @since 0.2
     * @param string $name
     * @return bool
     */
    public function CleanCache($name){
        if(DEBUG)
            return false;
        if($name){
            if($this->Core->Modules->Get('Multilang'))
                $name .= '_'.$this->Core->Multilang->Get();
            return memcache_delete($this->memcache, $name);
        }else
            memcache_flush($this->memcache);
    }
}
?>