<?php
if(!Defined('ROOT'))
    die('Access denied!');

class Cache {

    const CACHE_PATH = 'cache'; #Папка с кешем
    public $count = 0;#Кол-во файлов с данными вызванными из кеша

    function __construct(){
        
    }

    /**
     * Получение кеша
     * @access public
     * @since 0.1
     * @param string $name
     * @param int $timeout
     * @return string
     */
    public function Get($name, $timeout = 3600, $is_array = false)
    {
        if(DEBUG)
            return false;
        #if(Core::getInstance()->Getc('Modules')->Get('Multilang'))
        #    $name .= '_'.Core::getInstance()->Getc('Multilang')->Get();
        $cache_file = ENGINE.self::CACHE_PATH.'/'.$name.'.php';
        if(!file_exists($cache_file))
            return false;
        if(filemtime($cache_file) < (time() - $timeout))
            return false;
        $this->count++;
        if($is_array)
            return unserialize(file_get_contents($cache_file));
        else
            return file_get_contents($cache_file);
    }

    /**
     * Установка кеша
     * @access public
     * @since 0.1
     * @param string $name
     * @param $value
     */
    public function Set($name, $value)
    {
        if(DEBUG)
            return false;
        #if(Core::getInstance()->Getc('Modules')->Get('Multilang'))
        #    $name .= '_'.Core::getInstance()->Getc('Multilang')->Get();
        if(!is_dir(ENGINE.self::CACHE_PATH)){
            mkdir(ENGINE.self::CACHE_PATH);
            chmod(ENGINE.self::CACHE_PATH, 0777);
        }
        if(is_array($value))
            $value = serialize($value);
        $cache_file = ENGINE.self::CACHE_PATH.'/'.$name.'.php';
        $f = fopen($cache_file, "w+");
        chmod($cache_file, 0666);
        fwrite($f, $value);
        fclose($f);
    }

    /**
     * Очистка кеша
     * @access public
     * @since 0.1
     * @param string $name
     */
    public function CleanCache($name = false)
    {
        if(DEBUG)
            return false;
        if(!$name)
        {
            $handle = opendir(ENGINE.self::CACHE_PATH);
            while(($file = readdir($handle)) !== false)
                if($file !== '.' and $file !== '..' and $file !== '.htaccess')
                    $cache_files[] = $file;
            closedir($handle);
            foreach($cache_files as $file)
                @unlink(ENGINE.self::CACHE_PATH.'/'.$file.'.php');
        }else{
            if(Core::getInstance()->Getc('Modules')->Get('Multilang'))
                $name .= '_'.Core::getInstance()->Getc('Moultilang')->Get();
            @unlink(ENGINE.self::CACHE_PATH.'/'.$name.'.php');
        }
    }
}
?>