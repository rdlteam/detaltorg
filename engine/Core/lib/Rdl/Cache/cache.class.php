<?php
if(!Defined('ROOT'))
    die('Access denied!');

if(CACHE_DRIVER AND file_exists(ENGINE.'classes/Cache/'.CACHE_DRIVER.'_cache.class.php'))
    include_once ENGINE.'classes/Cache/'.CACHE_DRIVER.'.class.php';
else
    include_once ENGINE.'classes/Cache/rdl.class.php';
?>