<?php
namespace Rdl\Cache;

use Zend\Cache\Storage\Adapter\RedisOptions;
use Zend\Cache\Storage\Adapter\Redis;
use Zend\Serializer\Serializer;
use Zend\Cache\StorageFactory;

class RedisCache extends \Rdl\CoreFactory{
	
	private $Cache = false;
	private $redis_cache = false;

	function __construct($NameSpace = 'rdlcms', $adapter = 'filesystem'){
		$redis_options = new RedisOptions();
		$redis_options->setResourceId('RedisRDL')->setServer(array('host' => '127.0.0.1', 'port' => '6379'));
		return $this->redis_cache = StorageFactory::adapterFactory('Redis', $redis_options);
	}

	public function __call($method, $args){
		return call_user_func_array(array($this->redis_cache, $method), $args);
	}
}