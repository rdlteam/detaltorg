<?php
namespace Rdl\Auth;

use \Zend\Db\TableGateway\TableGateway;
use \Zend\Db\TableGateway\Feature\RowGatewayFeature;
use \Rdl\Request\Request;
use \Zend\Authentication\Adapter\DbTable as AuthAdapter;
use \Zend\Authentication\AuthenticationService;
use \Zend\Authentication\Storage\Session;
use \Zend\Crypt\Password\Bcrypt;
use \Rdl\Security\Security;
use \Rdl\Exception\AjaxException;
use DateTime;
class Auth extends \Rdl\CoreFactory {
	
	protected $Auth = false;
	protected $obAuth = false;
	protected $obUser = false;
	protected $obCrypt = false;

	protected $CookieUserID = 'rdl_user_id';
	protected $CookieUserHash = 'rdl_user_hash';
	
	protected $hashGenerate = false;
	public $arError = array();

	public function __construct(){
		parent::__construct();
		$this->obCrypt = new Bcrypt(array('salt' => $this->Core->Config->HashKey, 'cost' => 13));
		$this->Core->arExtraLog['method'] = 'auth'; 
	}

	public function Run(){
		// Метод для защиты от перебора паролей
		// $this->Security();

		if($this->isLogin() && Request::Get('logout') == 'Y') 
			$this->Logout();
		
		if($this->isLogin() && Request::Get('lock') == 'Y') 
			$this->Lock();

		if($this->isLogin() && !$this->isLock()){
			$this->obUser = new TableGateway('rdl_users', $this->Registry->DataBase);
			$this->obUser = $this->obUser->Select('id = '.(int) $this->Registry->SessionStorage->User['user_id'])->current();
			$this->Core->arExtraLog['login'] = $this->obUser->login;
			if(!$this->obCrypt->verify($this->obUser->id . $this->obUser->last_active . $this->obUser->password, $this->Registry->SessionStorage->User['user_hash'])){
				$this->Registry->Logger->info('Выход из-за отсутвие валидного хэша', $this->Core->arExtraLog);
				$this->Logout();
			}
		}

		if(Request::Post('user_login') AND Request::Post('user_password')){			
			
			Security::getInstance()->verifyOrFail();
			$this->Authorize();

		} else if($this->isLock() && Request::Post('user_password')){
			
			Security::getInstance()->verifyOrFail();
			$this->Authorize();

		} else if(isset($_COOKIE['rdl_user_id'], $_COOKIE['rdl_user_hash']) && !$this->isLogin()){
			if($this->Core->Config->remember == true)
				$this->Authorize(true);
			else
				$this->Logout();
		}
	}

	private function Authorize($remember = false){
		$this->Auth = new AuthAdapter($this->Registry->DataBase, 'rdl_users', 'auth_login', 'password', 'MD5(?)');
		
		$authType = $this->Core->Config->AuthType;
		
		if($this->isLock())
			$authType = 35;

		if($remember == true)
			$authType = 2;

		$user_login = '';
		switch((int) $authType){
			case 1:
				// Авторизация в качестве логина "Email"
				$this->Auth->setIdentityColumn('email');
				$this->Auth->setIdentity($this->Core->arExtraLog['login'] = Request::Post('user_login'))->setCredential(Request::Post('user_password'));
				$logMessage = array('success' => 'Успешная авторизация по email и паролю.', 'error' => 'Сочетание email и пароль не найдено');
				break;
			case 2:	
				// Восстановление авторизации "Запомнить меня"
				$this->Auth->setIdentityColumn('id')->setCredentialColumn('hash');
				$this->Auth->setIdentity($this->Core->arExtraLog['login'] = (int) $_COOKIE['rdl_user_id'])->setCredential($_COOKIE['rdl_user_hash']);
				$logMessage = array('success' => 'Успешное восстановление авторизации', 'error' => 'Ошибка при восстановлении авторизации сочетание Логин и Хэш не найдено');
				break;
			case 35:
				// Восстановление авторизации функция "Заблокировать"
				$this->Auth->setIdentityColumn('id');
				$this->Auth->setIdentity($this->Registry->SessionStorage->User['user_id'])->setCredential(Request::Post('user_password'));
				$this->Core->arExtraLog['login'] = $this->Registry->SessionStorage->User['user_login'];
				$logMessage = array('success' => 'Успешная разблокировка', 'error' => 'Ошибка при разблокировке');
				break;
			default:
				// Обычная авторизация
				$this->Auth->setIdentity($this->Core->arExtraLog['login'] = Request::Post('user_login'))->setCredential(Request::Post('user_password'));
				$logMessage = array('success' => 'Успешная авторизация по логину и паролю.', 'error' => 'Сочетание логин и пароль не найдено');
				break;
		}

		$this->obAuth = new AuthenticationService;
		$result = $this->obAuth->authenticate($this->Auth);
		
		if(!$result->isValid()){
			$this->ClearRemember();
			
			$this->Registry->Logger->err($logMessage['error'], $this->Core->arExtraLog);
			
			
			$this->arError['Code'] = 13001;
			$this->arError['Message'] = $logMessage['error'];
			
			if($this->Core->Service == 'ajax')
				throw new AjaxException($this->arError['Code'], $this->arError['Message']);
			
		} else {
			
			
			if(!$this->isLock()){
				$obDate = new DateTime();
				$lastActive = $obDate->format('Y-m-d H:i:s');
        		$this->obUser = $this->Auth->getResultRowObject();
        		$this->Registry->SessionStorage->Auth = true;
        		$this->Registry->SessionStorage->User = array(
        			'user_id' => $this->obUser->id, 
        			'user_hash' => $this->obCrypt->create($this->obUser->id . $lastActive . $this->obUser->password),
        			'user_login' => $this->obUser->login
        		);
        		
        		$this->hashGenerate = sha1($this->Registry->SessionStorage->User['user_hash']);

        		$this->UpdateHash($lastActive);
        		
        		$this->RememberMe();
        		
        	} else unset($this->Registry->SessionStorage->Lock);

        	if(intval($this->Core->arExtraLog['login']) > 0)
        		$this->Core->arExtraLog['login'] = $this->Registry->SessionStorage->User['user_login'];

        	if($this->Core->isAjax() && Request::Post('user_login'))
        		die(json_encode(array('Error' => 'false')));
        	

        	$this->Registry->Logger->info($logMessage['success'], $this->Core->arExtraLog);

			if($this->Core->Service != 'ajax' && Request::Post('request_page')){
        		header('Location: '. Request::Post('request_page'));
        		die();
        	}
        }
	}

	private function UpdateHash($lastActive){
		$obUser = new TableGateway('rdl_users', $this->Registry->DataBase, new RowGatewayFeature('id'));
		$arUser = $obUser->Select(array('id' => (int) $this->Registry->SessionStorage->User['user_id']))->Current();
		$arUser->hash = md5($this->hashGenerate);
		$arUser->last_active = $lastActive;
		$arUser->save();
	}

	private function RememberMe(){
		if($this->Core->Config->remember != true){ 
			$this->ClearRemember();
			return false;
		}

		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
		setcookie($this->CookieUserID, $this->Registry->SessionStorage->User['user_id'], time()+60*60*24*365, '/', $domain, false);
		setcookie($this->CookieUserHash, $this->hashGenerate, time()+60*60*24*365, '/', $domain, false);		
	}

	public function ClearRemember(){

		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
		if(isset($_COOKIE[$this->CookieUserID]))
			setcookie($this->CookieUserID, NULL, time()+60*60*24*365, '/', $domain, false);

		if(isset($_COOKIE[$this->CookieUserHash]))
			setcookie($this->CookieUserHash, NULL, time()+60*60*24*365, '/', $domain, false);
	}

	public function Logout(){
			if($this->Core->Service != 'index' && !$this->isLogin())
				return false;
			
			unset($this->Registry->SessionStorage->Auth);
			unset($this->Registry->SessionStorage->User);
			if(isset($this->Registry->SessionStorage->Lock))
				unset($this->Registry->SessionStorage->Lock);

			$this->ClearRemember();

			$Location = '/';
			if($this->Core->Service == 'admin')
				$Location .= $this->Core->Config->admin;
			
			if(Request::Get('return'))
				$Location .= Request::Get('return');

			header('Location: '. $Location);
			die();
	}

	public function Lock(){
		$this->Registry->SessionStorage->Lock = true;
		header('Location: /'. $this->Core->Config->admin .'/');
		die();
	}

	public function __get($field){
		if(!$this->isLogin())
			return false;

		return isset($this->obUser->{$field}) ? $this->obUser->{$field} : false;
	}

	public function isLogin(){
		return (isset($this->Registry->SessionStorage->Auth) && $this->Registry->SessionStorage->Auth === true) && !empty($this->obUser->id);
	}

	public function isLock(){
		return (isset($this->Registry->SessionStorage->Lock) && $this->Registry->SessionStorage->Lock === true);
	}

	public function isAdmin(){
		return isset($this->obUser) && $this->obUser['login'] == 'max';
	}
}