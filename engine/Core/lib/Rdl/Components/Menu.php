<?php
namespace Rdl\components;

use Zend\Cache\StorageFactory;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\Sql\Select;

class Menu extends \Rdl\CoreFactory {
	protected $Cache = false;
	protected $arReturn = array();
	public $CacheID = 'MenuCategoryCache';

	public function CacheRun(){
		$CacheID = 'Parent'.$this->CacheID;
		$this->arReturn['parent'] = json_decode($this->obCache->getItem($CacheID), true);
		if(empty($this->arReturn['parent'])){
			$obQuery = new TableGateway('rdl_menu', $this->Registry->DataBase);
			$arData = array();
			foreach($obQuery->Select(array())->toArray() as $arMenu){
				$this->arReturn['parent'][$arMenu['id']] = $this->arReturn['parent'][$arMenu['altername']] = $arMenu;
			}
	
			$this->obCache->setItem($CacheID, json_encode($this->arReturn['parent']));
		}

		$CacheID = 'Children'.$this->CacheID;
		$this->arReturn['menu'] = json_decode($this->obCache->getItem($CacheID), true);
		if(empty($this->arReturn['menu'])){
			$obQuery = new TableGateway('rdl_menu_line', $this->Registry->DataBase);
			$arData = array();
			foreach($obQuery->Select(array())->toArray() as $arMenu){
				$this->arReturn['menu'][$arMenu['menu_id']][$arMenu['id']] = $arMenu;
			}
	
			$this->obCache->setItem($CacheID, json_encode($this->arReturn['menu']));
		}
	}

	public function GetMenu($altername = false){
		if(empty($altername))
			return false;
		$altername = filter_var($altername, FILTER_SANITIZE_STRING);

		if(!isset($this->arReturn['parent'][$altername]))
			return "Not found";

		$arMenu['menu'] = $this->arReturn['parent'][$altername];
		$arMenu['line'] = isset($this->arReturn['menu'][$arMenu['menu']['id']]) ? $this->arReturn['menu'][$arMenu['menu']['id']] : false;

		return $arMenu;
	}

	public function CacheClear(){
		$this->obCache->removeItem('Parent'.$this->CacheID);
		return $this->obCache->removeItem('Children'.$this->CacheID);
	}


}