<?php
namespace Rdl\Components;

use Rdl\View\Template,
	ReflectionClass,
	Rdl\Components\ComponentLoader;

abstract class ControllerFactory extends \Rdl\CoreFactory {
	
	protected $arParams = array();
	protected $controllerPath = false;
	protected $ControllerName = false;
	protected $TypeComponent  = false;
	protected $cTemplate	  = false;
	protected $ControllerTemplate = false; 

	protected $obReflection = null;

	private $Model = false;

	final public function __construct($arParams = array(), $typeComponent = false){
		parent::__construct();

		$this->Template = &$this->Registry->Template;

		$this->obReflection = new ReflectionClass(get_called_class());
		$this->ControllerName = str_replace('Controller', '', $this->obReflection->getShortName());
		$this->controllerPath = dirname($this->obReflection->getFileName());

		$this->TypeComponent = preg_replace('#(.*?)/(.*?)/'. $this->ControllerName .'#isU', '$2', $this->controllerPath);
	}

	abstract protected function Options();

	public function ReloadControllerPath($template = 'default'){
		
		if(!empty($this->arParams['Template']))
			$template = $this->arParams['Template'];

		if(!$this->ControllerTemplate)
			$this->ControllerTemplate = new Template();

		$arPath = array(
				'template'	=> ENGINE_DIR . 'templates/' . $this->Core->Config->Site->Template .'/components/'. $this->TypeComponent .'/'.$this->ControllerName . '/' . $template .'/', 
				'component' => $this->controllerPath .'/templates/' . $template .'/'
			);

		foreach($arPath as $Key => $Path){
			if(is_dir($Path)) {
				$this->ControllerTemplate->_path = $Path;
				
				if($Key == 'component')
					$Path = 'engine/' . $Path;

				$this->ControllerTemplate->Template = $this->Core->Url . '/'. str_replace(ENGINE_DIR, '', $Path);
				break;
			}
		}
	}
	
	public function PathComponentTemplate(){
		return '';
	}

	public function LoadModel($model, array $arParams = array()){
		$obComponentLoad = new ComponentLoader;
		return $obComponentLoad->LoadModel($model, $arParams);
	}

	public function thisModel($reload = false){
		if(!$this->controllerPath || !$this->ControllerName) 
			return false;
			
		$obComponentLoad = new ComponentLoader;
		return $obComponentLoad->LoadModel($this->TypeComponent .':'. $this->ControllerName, $this->arParams);
	}	

	public function setParams(array $arParams = array()){
		$this->arParams = array_merge($this->arParams, $arParams);
		return $this;
	}
}