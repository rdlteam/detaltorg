<?php
namespace Rdl\Components\Catalog;

use Zend\Cache\StorageFactory;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\Sql\Select;
use Rdl\Request\Request;

class Catalog extends \Rdl\CoreFactory {
	protected $Cache = false;
	protected $arReturn = array();
	public $CacheID = 'CatalogCategoryCache';
	private static $arCategory = array();

	public function __construct(){
		parent::__construct();

		$this->CacheRun();
		$this->arReturn = json_decode($this->obCache->getItem($this->CacheID), true);
	}

	public function CacheRun(){
		if(!$this->obCache->getItem($this->CacheID)){
			$obQuery = new TableGateway('rdl_catalog_category', $this->Registry->DataBase);
			$obCategory = $obQuery->select(function (Select $select) {
     				$select->order('parent_category ASC');
				});
			$arData = array();
			foreach($obCategory->toArray() as $arCategory){
				$this->arReturn[$arCategory['id']] = $this->arReturn['alt'][$arCategory['altname']] = $arCategory;
				if($arCategory['parent_category'] > 0){
					$this->arReturn['parent_category'][$arCategory['parent_category']][$arCategory['id']] = $this->arReturn['alt'][$arCategory['parent_category']][$arCategory['altname']] = &$this->arReturn[$arCategory['id']];
				}
			}
	
			$this->obCache->setItem($this->CacheID, json_encode($this->arReturn));
		}
	}

	public function getCategoryId($id){
		if(intval($id) <= 0)
			return false;

		return isset($this->arReturn[$id]) ? $this->arReturn[$id] : false;
	}

	public function getCategoryName($name, $parent_category = false){
		$name = filter_var($name, FILTER_SANITIZE_STRING);
		if(mb_strlen($name) <= 0)
			return false;
		
		if($parent_category !== false)
			return isset($this->arReturn['alt'][$parent_category]['id'], $this->arReturn['alt'][$this->arReturn['alt'][$parent_category]['id']][$name]) ? $this->arReturn['alt'][$this->arReturn['alt'][$parent_category]['id']][$name] : false;
		else
			return isset($this->arReturn['alt'][$name]) ? $this->arReturn['alt'][$name] : false;
	}

	public function getCategorys($category){
		$name = filter_var($category, FILTER_SANITIZE_STRING);
		if(mb_strlen($name) <= 0 OR empty($this->arReturn['alt'][$name]['id']))
			return false;

		return isset($this->arReturn['alt'][$name]['id'], $this->arReturn['parent_category'][$this->arReturn['alt'][$name]['id']]) ? $this->arReturn['parent_category'][$this->arReturn['alt'][$name]['id']] : false;
	}

	public function GetCategoryList($category = ''){
		if(empty($category)){
			$arCategory = $this->arReturn;
			unset($arCategory['alt'], $arCategory['parent_category']);
			return $arCategory;
		}

		$category = filter_var($category, FILTER_SANITIZE_STRING);
		if(isset($this->arReturn[$category]))
			return $this->arReturn[$category];
		elseif(isset($this->arReturn['alt'][$category]))
			return $this->arReturn['alt'][$category];
		else
			return array();
	}

	public function GetTreeCategory($category_id, $seperator = ' - '){
		if($this->getCategoryId($category_id)['parent_category'] != 0)
			$this->GetTreeCategory($this->getCategoryId($category_id)['parent_category'], false);
		
		self::$arCategory[] =  $this->getCategoryId($category_id)['name'];

		if($seperator !== false){
			$arCategory = join($seperator, self::$arCategory);
			self::$arCategory = array();
			return $arCategory;
		}
	}

	public function CacheClear(){
		return $this->Cache->removeItem($this->CacheID);
	}


}