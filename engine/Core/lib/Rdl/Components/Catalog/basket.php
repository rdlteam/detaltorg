<?php
namespace Rdl\Components\Catalog;

use Zend\Cache\StorageFactory;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\Sql\Select;
use Rdl\Request\Request;

class Basket extends \Rdl\CoreFactory {
	static function CheckNumber($cnumber, $code = 5) {
		$number = array();
		for($i = 1; $i <= ($code - mb_strlen($cnumber)); $i++){
			$number[] = '0';
		}
		$number[] = $cnumber;
		return join('', $number);
	}
}