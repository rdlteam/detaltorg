<?php
namespace Rdl\Components;

class ComponentLoader extends \Rdl\CoreFactory {

	protected $controllerFile = 'controller.php';
	protected $modelFile = 'model.php';

	public function LoadModel($ModelName = '', array $arParams = array()){
		if(empty($ModelName))
			return false;

		$arModel = $this->parseController($ModelName);
		
		$pathModel = ENGINE_DIR .'components'. DIRECTORY_SEPARATOR . $arModel['namespace']. DIRECTORY_SEPARATOR . $arModel['name'] . DIRECTORY_SEPARATOR . $this->modelFile;

		if(!file_exists($pathModel))
			die('Не возможно загрузить модель '. $ModelName .', модель не существует');

		require_once $pathModel;
		$modelClass = $arModel['name'] . 'Model';

		if(!class_exists($modelClass))
			return 'Не возможно найти класс '. $modelClass .' данный класс не существует';

		$obModel = new $modelClass;

		if(count($arParams) > 0)
			$obModel->setParams($arParams);

		return $obModel;
	}


	public function Load($ControllerName = '', $Action = 'index', array $arParams = array(), $runMethod = 'Action'){
		if(empty($ControllerName))
			return false;

		$obTime = new \Rdl\Microtimer\Microtimer;
		$obTime->start();
		$arController = $this->parseController($ControllerName);
		
		if($this->Core->Service == 'admin' && $arController['namespace'] != 'Admin' && $runMethod != 'Include')
			$this->controllerFile = 'Admin' . ucfirst($this->controllerFile);

		$pathController = ENGINE_DIR . 'components'. DIRECTORY_SEPARATOR . $arController['namespace'] . DIRECTORY_SEPARATOR . $arController['name'] . DIRECTORY_SEPARATOR . $this->controllerFile;

		if(!file_exists($pathController))
			return false;

		require_once $pathController;
		$controllerClass = $arController['name'] . 'Controller';

		if(!class_exists($controllerClass))
			return false;

		$obClass = new $controllerClass;

		$Action = ucfirst($Action);
		$classAction = $Action . $runMethod;
		$BeforeClassAction = 'Before' . $classAction;
		$AfterClassAction = 'After' . $classAction;

		ob_start();
		if(count($arParams) > 0)
			$obClass->setParams($arParams);

		$obClass->Options();
		$obClass->ReloadControllerPath();

		if(method_exists($controllerClass, $BeforeClassAction))
			$obClass->{$BeforeClassAction}();

		if(method_exists($controllerClass, $classAction))
			$obClass->{$classAction}();

		if(method_exists($controllerClass, $AfterClassAction))
			$obClass->{$AfterClassAction}();

		$Return = ob_get_contents();

		$Return .="\n<!-- Время загрузки компонента ". $ControllerName ." составляет ". $obTime->get() ."-->\n";
		ob_end_clean();

		return $Return;
	}

	private function parseController($controllerName = ''){
		$arController = explode(':', $controllerName, 2);
		$arReturn = array(
			'namespace' => ucfirst($arController[0]),
			'name'	=> ucfirst($arController[1])
			);

		unset($arController);

		return $arReturn;
	}
}