<?php
namespace Rdl\Components;

abstract class ModelFactory extends \Rdl\CoreFactory {
	public $arParams = array();

	public function setParams(array $arParams = array()){
		$this->arParams = array_merge($this->arParams, $arParams);
		return $this;
	}
}
