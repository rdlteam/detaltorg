<?php
namespace Rdl\components;

use Zend\Cache\StorageFactory,
	Rdl\Route\Route,
	Rdl\Module\Standart,
	Rdl\Exception\ErrorPage,
	\FileSystemIterator,
	\RecursiveIteratorIterator,
	Rdl\Request\Request,
	Zend\Db\TableGateway\TableGateway,
	Rdl\Components\ComponentLoader;

class Loader extends \Rdl\CoreFactory {

	private static $instance = false;
	
	public $arPages = false;
	public $arPagesQuery = false;
	public $arAlternativePages = false;

	private $arControllers = false;

	public $PageParams = array();
	private $Cache = false;

	public $Content = '';
	public $PageContent = '';
	public $controllerContent = '';
	private $arController = array();
	static public function getInstance(){
		if(!self::$instance)
			self::$instance = new self;


		return self::$instance;
	}

	public function Run(){
		$this->LoadingRouteRule();

		$this->LoadingPage($this->Core->Service);
		$this->LoadingControllerBase();

		$this->Registry->Route->Run();


		if($this->Core->isAjax()){
			$page = array();
			
			if($GetController = Request::Get('controller'))
				$page['controller'] = $GetController;

			if($GetActionController = Request::Get('action'))
				$page['action']		= $GetActionController;

			if(empty($page) && !isset($this->arPages[$this->Registry->Route->Page]))
					throw new ErrorPage(404);

			$this->LoaderController(!empty($page) ? $page : $this->arPages[$this->Registry->Route->Page]);
			die();
		} else {	
			if(!isset($this->arPages[$this->Registry->Route->Page]))
				throw new ErrorPage(404);

			$this->LoaderController($this->arPages[$this->Registry->Route->Page]);
			$this->Core->pageTemplate = $this->arPages[$this->Registry->Route->Page]['template'];
		}
	}

	public function LoadingPage($service){
		$cacheID = 'pages-' . $service;

		$this->arPages = json_decode($this->obCache->getItem($cacheID), true);
		
		if(!$this->obCache->getItem($cacheID)){
			$obQuery = new TableGateway('rdl_pages', $this->Registry->DataBase);
			$arPagesStack = array();
			foreach($obQuery->select(array('service' => mb_strtolower($service)))->toArray() as $arPage){
				
				if($arPage['parent'] > 0 && !empty($arPagesStack[$arPage['parent']])){
					$arPage['url'] = $arPagesStack[$arPage['parent']]['url'] . $arPage['url'];
				}
	
				$arPagesStack[$arPage['id']] = $arPage;

				$hashPage = sha1(trim($arPage['url']));
				$this->arPagesQuery[$hashPage] = &$arPagesStack[$arPage['id']];
				
				if(mb_strpos(trim($arPage['url']), '.php') === false && mb_strpos(trim($arPage['url']), '.html') === false){
					$this->arAlternativePages[sha1(trim($arPage['url'] . 'index.html'))] = &$this->arPagesQuery[$hashPage];
				}
	
			}
	
			$this->arPages = array_merge(empty($this->arPagesQuery) ? array() : $this->arPagesQuery, empty($this->arAlternativePages) ? array() : $this->arAlternativePages);
			unset($arPagesStack);
	
			$this->obCache->setItem($cacheID, json_encode($this->arPages));
		} 
		

		if(is_array($this->arPages))
			$this->Registry->Route->arPages = array_keys($this->arPages);
	}

	private function LoadingControllerBase(){
		$obQuery = new TableGateway('rdl_controllers', $this->Registry->DataBase);
		foreach($obQuery->select()->toArray() as $arController){
			$this->arControllers[ucfirst($arController['prefix'])][$arController['name']] = $arController;
		}
	}
	
	private function LoadingRouteRule(){
		return false;
		foreach(new \Rdl\Loader\SplRecursive(ENGINE_DIR .'components/', 'php') as $entry) {
	    	$this->Registry->Route->RouteRule = array_merge($this->Core->Route->RouteRule, include_once($entry->getRealPath()));
		}
	}

	private function LoaderController(array $page = array()){

		$this->Registry->Template->pageTitle = isset($page['title']) ? $page['title'] : '';

		$this->Registry->Template->PageContent = isset($page['page']) ? $this->LoadingPagePlagins($page['page']) : '';
		
		if(!empty($page['keyword']))
			$this->Core->otherKeywords = join(',', array_unique(explode(',', $page['keyword'])));
		
		if(!empty($page['desc']))
			$this->Core->otherDescription = filter_var($page['desc'], FILTER_SANITIZE_STRING);
		
		if(empty($page['controller'])){
			$this->Registry->Helper->headTitle()->append(isset($page['title']) ? $page['title'] : '');
			$this->Registry->Template->title[] = $this->Registry->SpeedBar[(isset($page['url']) ? $page['url'] : '')] = $this->Registry->Template->pageTitle;
			$this->Registry->Template->Content .= $this->Registry->Template->PageContent;
			return;
		}

		$obController = new ComponentLoader;
		$this->Registry->Template->controllerContent = $obController->Load($page['controller'], (!empty($page['action']) ? $page['action'] : 'index'), array(), ($this->Core->isAjax() ? 'AjaxAction' : 'Action'));
		$this->Registry->Template->Content .= $this->Registry->Template->PageContent . $this->Registry->Template->controllerContent;
	}

	private function LoadingPagePlagins($data){
		if(mb_strpos($data, "[include=") !== false){
			
		}

		return $data;
	}

	public function getControllerName(){
		return array(
			'controller' => $this->arController[0] .':'. $this->arController[1], 
			'namespace' => $this->arController[0], 
			'controllerName' => $this->arController[1]);
	}
}