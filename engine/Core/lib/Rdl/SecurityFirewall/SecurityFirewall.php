<?php
/**
* RDL CMS SecurityFirewall
* Класс обертка над библиотекой phpIDS
*
* @link      http://www.rdl-team.ru/ official site
* @copyright Copyright (c) 2009-2014 RDL Team. (http://www.rdl-team.ru)
* @license   http://www.rdl-team.ru/license/new-bsd New BSD License
*/

namespace Rdl\SecurityFirewall;

use IDS\Init as Init,
	IDS\Monitor,
	Rdl\Request\Request,
	Zend\Db\TableGateway\TableGateway,
	Zend\Db\TableGateway\Feature\RowGatewayFeature;

class SecurityFirewall extends \Rdl\CoreFactory{
	/** 
	* @param array $arRequests 
	*/
	protected $arRequests = array();
	
	/**
	* @param bool | object $obIDS
	*/
	protected $obIDS = false;

	/**
	* @param bool | object $instance
	*/
	private static $instance = false;
	
	const FIREWALL_DIR = 'cache/firewall/';
	const FIREWALL_CACHE = 'default_filter.cache';

	const CACHE_ID = 'FirewallRuleCache';

	protected $arExceptions = array();
	
	public static function getInstance(){
		if(!self::$instance) 
			self::$instance = new self;

		return self::$instance;
	}

	public function __construct(){
		parent::__construct();
		// определяем параметры фильтра и входящие данные
		$this->arRequest = array(
			'REQUEST' => Request::GetParam(array('method' => 'REQUEST')),
			'GET' 	  => Request::GetParam(array('method' => 'GET')),
			'POST' 	  => Request::GetParam(array('method' => 'POST')),
			'COOKIE'  => $_COOKIE
		);
		$this->obIDS = Init::init(ENGINE_DIR . 'Core/lib/IDS/Config/Config.ini');
		
		$this->ConfigureFirewall();
  		$this->obIDS->config['General']['base_path'] = ENGINE_DIR . 'Core/lib/IDS/';
		$this->obIDS->config['General']['use_base_path'] = true;
	}

	/**
	* Инициализация фаервола
	* @author LeMaX (rdl-team.ru)
	* @return void
	*/
	public function Run(){
		// Поиск исключений
		$this->SearchExceptions();
		// Инициализация стандартных исключений
		$this->LoadSystemExceptions();
		// Поиск и отлов ошибок
		$this->SearchError();
	}

	/**
	* Поиск исключений в хранилище
	* @author LeMaX (rdl-team.ru)
	* @return void
	*/
	private function SearchExceptions(){
		// Проверка на наличие закешированной версии
		if(!$arData = $this->obCache->getItem(self::CACHE_ID)){
			$obTable = new TableGateway('rdl_firewall', $this->Registry->DataBase);
			$obRow  = $obTable->Select(array('lock' => 0));
			$arDataCache = array();
			while($arRow = $obRow->current()){
				$this->ExceptionRule($arDataCache[] = $arRow);
			}
			$this->obCache->setItem(self::CACHE_ID, json_encode($arDataCache));
			unset($arDataCache);
			return false;
		}

		foreach(json_decode($arData) as $key => $value){
			$this->ExceptionRule($value);
		}
		unset($arData);
	}	
	
	/**
	* Создание правил исключения
	* @author LeMaX (rdl-team.ru)
	* @return void
	*/
	private function ExceptionRule($arRow){
		$exception = mb_strtoupper($arRow->method) . '.' . $arRow->value;
		if(mb_strtolower($arRow->service) != 'all'){
			if($this->Core->Service == $arRow->service)
				$this->obIDS->config['General']['exceptions'][] = &$exception;
		} else 
			$this->obIDS->config['General']['exceptions'][] = &$exception;
	}
	/**
	* Инициализация стандартных исключений заданных в систему
	* @author LeMaX (rdl-team.ru)
	* @return void
	*/
	private function LoadSystemExceptions(){
		if(!is_array($this->arExceptions) || count($this->arExceptions) <= 0) return false;
		foreach($this->arExceptions as $Key => $Value){
			$this->obIDS->config['General']['exceptions'][] = $Value;
		}
	}

	/**
	* Метод для отлова ошибок, служит для инициализации монитора действия данных для  защиты системы
	* @author LeMaX (rdl-team.ru)
	* @return void
	*/
	private function SearchError(){
		$this->Registry->Security = new Monitor($this->obIDS);
		if(!$this->Registry->Security->run($this->arRequest)->isEmpty()) {

			throw new Exception\ErrorPage;
		}
	}

	/**
	* Метод для переконфигурирования мест хранения кеша фаервола
	* @author LeMaX (rdl-team.ru)
	* @return void
	*/
	private function ConfigureFirewall(){
		// Проверяем и создаем раздел кеширования
		if(!is_dir(ENGINE_DIR . self::FIREWALL_DIR)){
			mkdir(ENGINE_DIR . self::FIREWALL_DIR);
			chmod(ENGINE_DIR . self::FIREWALL_DIR, 0777);
		}

		// Переводим раздел на верхний уровень, изменяя вложенность
		$this->obIDS->config['General']['tmp_path'] = '../../../'. self::FIREWALL_DIR;
		$this->obIDS->config['Caching']['path'] = '../../../'. self::FIREWALL_DIR . self::FIREWALL_CACHE;
	}

	public function AddExceptionRule($method, $value){
		$Rule = join('.', array($method, $value));
		$this->arExceptions[$Rule] = $Rule;
	}

	public function DeleteExceptionRule($method, $value){
		$Rule = join('.', array($method, $value));
		unset($this->arExceptions[$Rule]);
	}
}