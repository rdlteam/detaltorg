<?php
/*
* RDL CMS created by RDL Team
* http://rdl-team.ru
* Copyright (c) 2009-2014 RDL Team
* Данный код защищен авторскими правами
*
* Файл: Core.php
* Назначение: Ядро системы
*/
if(!defined("SECURITY")) 
	header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);


/* Session */
use Rdl\Session\Session,
	Zend\Db\TableGateway\TableGateway,
	Zend\Session\SaveHandler\DbTableGateway,
	Zend\Session\SaveHandler\DbTableGatewayOptions,
	Zend\Session\Config\StandardConfig,
	Zend\Session\SessionManager,
	Zend\Session\Validator\RemoteAddr,
	Zend\Session\Validator\HttpUserAgent,
	Zend\Session\Container;

/* Main */
use Zend\Config\Config,
	Rdl\Components\Loader,
	Rdl\Loader\AutoLoader,
	Rdl\Version\Version,
	Rdl\helpers\helper,
	Zend\Debug\Debug,
	Rdl\Panel\Panel;

/* Template & Helpers*/
use Zend\View\Model\ViewModel,
	Zend\View\Renderer\PhpRenderer,
	Rdl\View\Template;

/* Security */
use Rdl\Request\Request,
	Rdl\SecurityFirewall\SecurityFirewall;

use Zend\Http\PhpEnvironment\RemoteAddress;
Final class RDLCore {
	private static $instance = false;

	public $Config = array();
	public $Url = false;

	public $Service = 'index';
	
	private $arService = array(
		'ajax' => 'ajax', 
		'thumbs' => 'thumbs'
	);

	public $InitObject = array(
				'Auth' 		=> array('object' => 'Rdl\Auth\Auth', 'RunMethod' => 'Run'),
				'SpeedBar' 	=> '\ArrayObject',
				'Route' 	=> 'Rdl\Route\Route',
				'Template' 	=> 'Rdl\View\Template',
				'Helper'	=> 'Zend\View\Renderer\PhpRenderer'
			);

	public $subTitle = '';

	public $pageTemplate = false;
	public $DefaultTemplate = false;
	public $title = false;
	
	public $arExtraLog = array();
	public $arLogCode = array(
			
		);

	const CORECACHE_DIR = 'cache';
	
	// Альтернативный title
	public $otherTitle = false;
	// Альтернативное описание
	public $otherDescription = false;
	// Алтернативные ключевые слова
	public $otherKeywords = false;

	public static function getInstance($Service = false){
		if(!self::$instance) 
			self::$instance = new self;

		if($Service !== false)
			self::$instance->Service = &$Service;

		return self::$instance;
	}

	static $MicroTimer = false;

	function __construct(){
		
		date_default_timezone_set('Europe/Moscow');

		header('Content-Type: text/html; charset=utf-8');		
		
		require_once ENGINE_DIR. 'Core/lib/Rdl/Registry/Registry.php';

		Rdl\Microtimer\Microtimer::getInstance()->start();

		$this->Config = new Config(require_once(ENGINE_DIR .'Core/config/global.config.php'), true);
		$this->DefaultTemplate = $this->Config->Site->Template;
		$this->Url = $this->Config->get('url', $this->Config->protocol . $_SERVER['HTTP_HOST']);

		$this->InitDirectory();
	}

	private function InitDirectory(){
		if(!is_dir(ENGINE_DIR . self::CORECACHE_DIR)){
			mkdir(ENGINE_DIR . self::CORECACHE_DIR);
			chmod(ENGINE_DIR . self::CORECACHE_DIR, 0777);
		}
	}

	/**
	* Публичный метод для инициализации системы
	* @author LeMaX (rdl-team.ru)
	*/
	public function Run(){
		// Переопределение сервиса для запуска
		$this->ReloadService();

		if(!$this->Service)
			throw new RuntimeException('Initialization Error service');



		// Проверка на сервис превью картинок
		if($this->Service == $this->arService['thumbs']){
			SecurityFirewall::getInstance()->Run();
			$this->ThumbsService();	

			// Прекращаем запуск сайта, отдаем изображение
			return false;
		}

		Registry::getInstance()->DataBase = new Zend\Db\Adapter\Adapter(array(
			'driver' => $this->Config->db->driver,
    		'database' => $this->Config->db->database,
    		'username' => $this->Config->db->username,
    		'password' => $this->Config->db->password,
    		'charset'  => 'utf8',
    			'driver_options' => array(
					\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET \'UTF8\'',
					\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
				)
    		)
 		);
		
		$this->InitLogger();
		
		$this->SessionStart();
		
		// Инициализация FireWall
		SecurityFirewall::getInstance()->Run();

		// Заменяем шаблон для админского сервиса
		if($this->Service == 'admin')
			$this->Config->Site->Template = 'adminv3';

		foreach($this->InitObject as $RegistryClass => $Object){
			if(is_array($Object)){
				Registry::getInstance()->$RegistryClass = new $Object['object'];
				Registry::getInstance()->$RegistryClass->{$Object['RunMethod']}();
			} else 
				Registry::getInstance()->$RegistryClass = new $Object;
		}

		Registry::getInstance()->Helper->headTitle($this->Config->Site->title)->setSeparator($this->Config->Site->Seperator);

		Loader::getInstance()->Run();

		switch(mb_strtolower($this->Service)){
			case 'index' :

				$this->HeaderGenerate();
				break;
			case 'admin' :
				if(Registry::getInstance()->Auth->isLock()){
					Registry::getInstance()->Helper->headTitle()->append('Заблокировано');
					$this->pageTemplate = 'locked';
				} else if(!Registry::getInstance()->Auth->isLogin()){
					$this->pageTemplate = 'login';
				}

				$this->HeaderGenerate();
				break;
		}
	}

	/**
	* Приватный метод для инициализации сессий
	* @author LeMaX (rdl-team.ru)
	*/
	private function SessionStart(){
		$SessionConfig = new StandardConfig();
		$SessionConfig->setOptions(array(
		   'remember_me_seconds' => 2419200,
		   'cookie_lifetime'     => 2419200,
		   'use_cookies'		 => true,
		   'cookie_secure'		 => true,
		   'cookie_httponly'	 => true,
		   'use_trans_sid'		 => false,
		   'name'                => 'rdl'
		   )
		);

		Registry::getInstance()->Session  = new SessionManager($SessionConfig);
		if($this->Config->Session->Storage == 'base'){
			$tableGateway = new TableGateway('rdl_session', Registry::getInstance()->DataBase);
			$saveHandler  = new DbTableGateway($tableGateway, new DbTableGatewayOptions());
			Registry::getInstance()->Session->setSaveHandler($saveHandler);
		}

		if($this->Config->Session->Validate == 'remoteaddr')
			Registry::getInstance()->Session->getValidatorChain()->attach('session.validate', array(new RemoteAddr(), 'isValid'));
		else if($this->Config->Session->Validate == 'httpuseragent')
			Registry::getInstance()->Session->getValidatorChain()->attach('session.validate', array(new HttpUserAgent(), 'isValid'));

		Registry::getInstance()->Session->start();
		Registry::getInstance()->SessionStorage = new Container('SessionData', Registry::getInstance()->Session);
		if (!isset(Registry::getInstance()->Session->init) && $this->Config->Session->Remember == true) {
             Registry::getInstance()->Session->regenerateId(true);
             Registry::getInstance()->SessionStorage->init = 1;
        }
	}

	/**
	* Метод для инициализации метода логирования данных
	* @author LeMaX (rdl-team.ru)
	*/

	private function InitLogger(){
		
		$this->arExtraLog['ip'] = $this->GetIP(new RemoteAddress);
		$this->arExtraLog['service'] = $this->Service;

		if($this->Config->Logger != 1){
			// Условие на случай отключенных логов... логи не пишем
			Registry::getInstance()->Logger = new Zend\Log\Logger();
			Registry::getInstance()->Logger->addWriter(new Zend\Log\Writer\Null);
			return false;
		}

		$mapping = array(
    		'timestamp' => 'date',
    		'priority'  => 'type',
    		'message'   => 'event',
		);

		Registry::getInstance()->Logger = new Zend\Log\Logger();
		Registry::getInstance()->Logger->addWriter(new Zend\Log\Writer\Db(Registry::getInstance()->DataBase, 'rdl_logger'));
	}
	/**
	* Приватный метод инициализации мета тегов и глобального шаблона сайта
	* @author LeMaX (rdl-team.ru)
	*/
	private function HeaderGenerate(){

		Registry::getInstance()->Helper->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset='. mb_strtoupper($this->Config->charset));
		Registry::getInstance()->Helper->headMeta()->appendName('generator', 'RDL CMS (http://rdl-team.ru)');

		if($this->otherDescription != false)
			Registry::getInstance()->Helper->headMeta()->appendName('description', $this->otherDescription);

		if($this->otherKeywords != false)
			Registry::getInstance()->Helper->headMeta()->appendName('keywords', $this->otherKeywords);
			

		if(count($this->Config->Site->meta) > 0) foreach($this->Config->Site->meta as $Name => $Value){
				if(!empty($this->{'other' . ucfirst($Name)})) continue;
				Registry::getInstance()->Helper->headMeta()->appendName($Name, $Value);
			}

		Registry::getInstance()->Template->Set('content', Registry::getInstance()->Template->Content);
	
		Registry::getInstance()->Template->Set('Header', Registry::getInstance()->Helper->headMeta() . "\n");
		Registry::getInstance()->Template->Set('Title',  Registry::getInstance()->Helper->headTitle() . "\n");
		Registry::getInstance()->Template->Display($this->pageTemplate);
		Registry::getInstance()->Template->Clear();
	}

	/**
	* Приватный метод для отдачи изображений по шаблону
	* @author LeMaX (rdl-team.ru)
	*/
	private function ThumbsService(){
		
		$thumb = Bazalt\Thumbs\Image::generateThumb(ROOT_DIR . Request::Get('thumb_file', true), new Bazalt\Thumbs\Operations());
		if ($thumb) {
		    switch (pathinfo($thumb, PATHINFO_EXTENSION)) {
			    case 'png':
		    	    header('Content-Type: image/png');
		        	break;
		    	case 'jpg':
		        	header('Content-Type: image/jpeg');
		        	break; 
		    }
		    
		    readfile($thumb);
		
		die();
		}
	}

	/**
	* Публичный метод для проверки на AJAX соединение
	* @author LeMaX (rdl-team.ru)
	*/
	public function isAjax(){
		return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH'], 'UTF-8') == 'xmlhttprequest');
	}

	/**
	* Приватный метод для инициализации сервиса систем
	* @author LeMaX (rdl-team.ru)
	*/
	private function ReloadService(){
		if(mb_strpos(rawurldecode($_SERVER['REQUEST_URI']), $this->Config->admin . '/') !== false){
			$this->Service = 'admin';
			return false;
		}

		if($this->isAjax()){
			$this->Service = $this->arService['ajax'];
			return false;
		}

		if(Request::Get('thumb_file')){
			$this->Service = $this->arService['thumbs'];
			return false;
		}
	}

	/**
	* Определение IP адреса посетителя, ориентируясь на наличие прокси и т д
	* @author LeMaX (rdl-team.ru)
	*/

	public function GetIP(RemoteAddress $ip){
		return $ip->getIpAddress();
	}
}