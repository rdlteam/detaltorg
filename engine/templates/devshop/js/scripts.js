$(document).ready(function(){

$('a.remember').click(function(){
  $('.popupBg').fadeIn();
  var heightWin = $('.popupBg .win').innerHeight();
  $('.popupBg .win').css('marginTop', -heightWin/2);
  var widthWin = $('.popupBg .win').innerWidth();
  $('.popupBg .win').css('marginLeft', -widthWin/2);
  var heightBody = $('body').outerHeight();
  if(heightBody < heightWin) {
	$('body').removeClass('noscroll').height(heightWin+100);
	$('.popupBg').height(heightWin+100).addClass('scroll');
  }
});

/*------------------------------------------------------------*/
/*	POPUP
/*------------------------------------------------------------*/

var heightBody = $('body').outerHeight();
var heightFootCat = $('.bottomCategories').height() + 40;
if ($('.bottomCategories').length == 0) heightFootCat = 0;
var heightFooter = heightFootCat + 50;
$('#footer').height(heightFooter);
$('#footer').css('marginTop', -heightFooter);
$('#empty').height(heightFooter);

var heightWhitePage = $('#whitePage').outerHeight();
if ((heightWhitePage < (heightBody - heightFooter)) && $('.commentsBlock').length == 0) {
	$('#whitePage').height(heightBody - heightFooter - 31);
}

$('.popupBg').hide();
$('.popupBg').find('.close').click(function(){
	$('.popupBg').fadeOut();
});

/*------------------------------------------------------------*/
/*	NUMBER INPUTS
/*------------------------------------------------------------*/
$('.switch').each(function() {
	$(this).find('.switch_top').click(function() {
		var val = Number($(this).parent().find('input').val());
		val = val+1;
		if (val > 99) {
			val = 99;
		}
		$(this).parent().find('input').val(val);
		$(this).parent().find('input').change();
	});
	$(this).find('.switch_bottom').click(function() {
		var val = Number($(this).parent().find('input').val());
		val = val-1;
		if (val < 1) {
			val = 1;
		}
		$(this).parent().find('input').val(val);
		$(this).parent().find('input').change();
	});
});
setValidator('.switch input', /^[0-9_]*$/);
/*------------------------------------------------------------*/
/*	INPUTS
/*------------------------------------------------------------*/
var formDefaults = Array();
$("input:not('.sub')").focusin(function() {
    if (formDefaults[$(this).attr('name')] == undefined) {
    	formDefaults[$(this).attr('name')] = $(this).attr('value');
    }
    if (formDefaults[$(this).attr('name')] == $(this).attr('value')) $(this).val('');
});
$("input:not('.sub')").focusout(function() {
    if (($(this).val() == '') || ($(this).val() == ' ')) $(this).val(formDefaults[$(this).attr('name')]);
});
$("textarea:not('.sub')").focusin(function() {
    if (formDefaults[$(this).attr('name')] == undefined) {
    	formDefaults[$(this).attr('name')] = $(this).attr('value');
    }
    if (formDefaults[$(this).attr('name')] == $(this).attr('value')) $(this).val('');
});
$("textarea:not('.sub')").focusout(function() {
    if (($(this).val() == '') || ($(this).val() == ' ')) $(this).val(formDefaults[$(this).attr('name')]);
});
/*------------------------------------------------------------*/
/*	JQTRANSFORM
/*------------------------------------------------------------*/
$('form').jqTransform();
/*------------------------------------------------------------*/
/*	LOGIN
/*------------------------------------------------------------*/
$('span.login').toggle(function(){
	pos = $(this).offset();
	w=$(this).width();
	h=$(this).height();
	$('#header .loginBlock form.login').css('left',(pos.left-$(window).width()/2-490)+w/2+'px');
	$('#header .loginBlock form.login').css('top',(pos.top-30)+h/2+'px');
	$('#header .loginBlock form.login').fadeIn();

},function(){
	$('#header .loginBlock form.login').fadeOut();
});

/*------------------------------------------------------------*/
/*	TOP MENU
/*------------------------------------------------------------*/
$('#header .navPanel > ul > li').each(function(i){
	i++;
	var parent = $(this);
	if(parent.find('.sub').length>0)
	{
	  parent.find('.sub').show();
	  var winWidth = $(window).width();
	  var left = parent.find('.sub').offset().left;
	  var subWidth = parent.find('.sub').outerWidth(true);
	  var offset = winWidth - (left + subWidth);
	  parent.find('.sub').hide();
	  if (offset < 0) {
		parent.find('.sub').css({
			'left' : offset,
			//'right' : 0
		});
	 }

	  parent.hover(function(){
	  	parent.addClass('hover').find('.sub').show();
	  },function(){
	  	parent.removeClass('hover').find('.sub').hide();
	  });
	}
});
/*------------------------------------------------------------*/
/*	MAIN SLIDER
/*------------------------------------------------------------*/
var root = $(".mainSlider .scrollable").scrollable({circular: true}).navigator().autoscroll({interval: 4000});
window.api = root.data("scrollable");
$(".mainSlider a").click (function(){
    api.stop()
});
/*------------------------------------------------------------*/
/*	SLIDER NEWS
/*------------------------------------------------------------*/
$(".sliderTextBlock.news .content").jCarouselLite({
    btnNext: ".sliderTextBlock.news .next",
    btnPrev: ".sliderTextBlock.news .prev",
	visible: 1
});
/*------------------------------------------------------------*/
/*	SLIDER BLOG
/*------------------------------------------------------------*/
$(".sliderTextBlock.blog .content").jCarouselLite({
    btnNext: ".sliderTextBlock.blog .next",
    btnPrev: ".sliderTextBlock.blog .prev",
	visible: 1,
	speed: 800
});
/*------------------------------------------------------------*/
/*	SLIDER BRANDS
/*------------------------------------------------------------*/
$(".brandsCarousel .inner > div").jCarouselLite({
    btnNext: ".brandsCarousel .next",
    btnPrev: ".brandsCarousel .prev",
	visible: 9,
	scroll: 9,
	speed: 500
});
/*------------------------------------------------------------*/
/*	MAIN TABS
/*------------------------------------------------------------*/
$(".mainTabsBlock ul.tabs").tabs("div.panes > div");
$(".mainTabsBlock .scrollable").scrollable({circular: true});
$(".typicalCarousel .scrollable").scrollable({circular: true});


/*------------------------------------------------------------*/
/*	STARS
/*------------------------------------------------------------*/

$('ul.voting li a').click(function(){
  $("ul.voting li a").removeClass('cur');
  $(this).addClass('cur');
  $('#vote').val(this.innerHTML);
  return false;
});

/*
$(".mainTabsBlock .scrollable").scrollable({circular: true});

// Get the Scrollable control
var scrollable = $(".mainTabsBlock .scrollable").data("scrollable");

// Set to the number of visible items
var size = 3;

// Handle the Scrollable control's onSeek event
scrollable.onSeek(function(event, index) {

// Check to see if we're at the end
if (this.getIndex() >= this.getSize() - size) {

  // Disable the Next link
  $("a.next").addClass("disabled");

}

});

// Handle the Scrollable control's onBeforeSeek event
scrollable.onBeforeSeek(function(event, index) {

// Check to see if we're at the end
if (this.getIndex() >= this.getSize() - size) {

  // Check to see if we're trying to move forward
  if (index > this.getIndex()) {

	// Cancel navigation
	return false;

  }

}

});



$('.mainTabsBlock .scrollable').each(function (i) {  // loop through scrollers
	    var thisCarousel = $('.mainTabsBlock .scrollable:eq(' + i + ')');  // var this scroller
	    var size = 5;
	    var bucketCount = $(thisCarousel).find('.item').length; // get number of buckets in this scroller
	    var thisNext = $(thisCarousel).parent().find('a.next'); // var this next controller
	    if (bucketCount < size + 1) { // hide next controller if less than size
	        thisNext.addClass('disabled');
	    }
	    $(thisCarousel).scrollable();
	    var scrollable = $(thisCarousel).data("scrollable");
	    scrollable.onSeek(function () {
	        if (this.getIndex() >= this.getSize() - size) {
	            thisNext.addClass('disabled');
	        }
	    });
	    scrollable.onBeforeSeek(function (event, index) {
	        if (this.getIndex() >= this.getSize() - size) {
	            if (index > this.getIndex()) {
	                return false;
	            }
	        }
	    });
	});
*/
/*------------------------------------------------------------*/
/*	SCROLLPANE
/*------------------------------------------------------------*/
$('.leftSidebar form.search .parametrBlock .scrollPane').jScrollPane();

/*------------------------------------------------------------*/
/*	CHANGE FIELDS
/*------------------------------------------------------------*/
$('form.greyBox .address .row').each(function(i){
	i++;
	var parent = $(this);
	parent.find('span.change').click(function(){
		$('.popupBg').fadeIn();
	});
});
$('form.greyBox .row .value').each(function(i){
	i++;
	var parent = $(this);
	parent.find('span.change').click(function(){
		$(this).hide(0);
		parent.find('.changeField').slideDown();
	});
});

/*------------------------------------------------------------*/
/*	CABINET TABS
/*------------------------------------------------------------*/
$('.cabinetTabs ul.tabs').delegate('li:not(.current)', 'click', function() {
	$(this).addClass('current').siblings().removeClass('current').parents('.cabinetTabs div.section').find('div.box').hide().eq($(this).index()).fadeIn(150);
});
/*------------------------------------------------------------*/
/*	TABLE ZEBRA
/*------------------------------------------------------------*/
$('.cabinetTabs table tbody tr:even').addClass('colored');
$('.popupBg .sizesChart table tbody tr:even').addClass('colored');
/*------------------------------------------------------------*/
/*	EXPAND BLOCKS
/*------------------------------------------------------------*/
$('.productBlock .prodDescribe .expandBlock').each(function(i){
	i++;
	var parent = $(this);
	parent.find('.title').click(function(){
		$('.productBlock .prodDescribe .expandBlock .title').removeClass('open');
		$('.productBlock .prodDescribe .expandBlock .content').slideUp();
		if (parent.find('.content').is(':hidden')) {
			$(this).addClass('open');
			parent.find('.content').slideDown();
			$('.productBlock .prodDescribe .expandBlock .content .scrollPane').jScrollPane();
		} else {
			$(this).removeClass('open');
			parent.find('.content').slideUp();
		}
	});
	/*
	parent.find('.title').toggle(function(){
		$(this).addClass('open');
		parent.find('.content').slideDown();
		$('.productBlock .prodDescribe .expandBlock .content .scrollPane').jScrollPane();
	},function(){
		$(this).removeClass('open');
		parent.find('.content').slideUp();
	});
	*/
});
/*------------------------------------------------------------*/
/*	SLIDESHOW
/*------------------------------------------------------------*/
$("a[rel=example_group]").fancybox({
	'transitionIn'		: 'none',
	'transitionOut'		: 'none',
	'titlePosition' 	: 'over'
});

$("a.fancybox").fancybox();

$(".productBlock .previews .smalls .small img").click(function() {
	var src = $(this).attr('src');
	$(".productBlock .previews .big .img").each(function(i, elem){
		if ($(this).find('img').attr('src') == src) {
			$(".productBlock .previews .big .img").removeClass('selected');
			$(this).addClass('selected');
		}
	});
});

/*---------------------------------------------------------------*/

});
/*---------------------------------------------------------------*/

function setimage(seltext)
{
  simg=null;sl=0;
  $(".productBlock .previews .big .img").each(function(i, elem)
  { title=$(this).attr('title');
    if(title!='' && seltext.replace(/\s+/,'').indexOf(title.replace(/\s+/,''))>=0)
    { if(title.replace(/\s+/,'').length>sl)
      { simg=$(this);
        sl=title.replace(/\s+/,'').length;
      }
    }
  });
  if(simg)
  { $(".productBlock .previews .big .img").removeClass('selected');
    simg.addClass('selected');
  }
}

function setValidator(id, regex) {
	$(id).each(function(){
		var interval;
		$(this).focus(function() {
			var field = $(this);
		    var lastValue = field.val();
		    if (!regex.test(lastValue)) {
				lastValue = '';
			}
			interval = setInterval(function() {
				var value = field.val();
				if (value != lastValue) {
					if (regex.test(value)) {
						lastValue = value;
					} else {
						field.val(lastValue);
					}
				}
			}, 10);
		});
		$(this).blur(function() {
			clearInterval(interval);
		});
	});
}









