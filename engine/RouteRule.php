<?php

return array(
		'#^/catalog/(.*?)/(.*?)/?([0-9]+)?/?$#i' => array(
			'page' => '/catalog/', 
			'controller' => 'Engine:catalog', 
			'Action' => 'Index', 
			'params' => array('parent_category' => '1', 'category' => '2', 'ID' => '3')
		)
		// '#^/basket/(.*?)/?$#i' => array(
		// 	'page' => '/basket/', 
		// 	'controller' => 'Engine:basket', 
		// 	'Action' => 'Index', 
		// 	'params' => array('step' => 1)
		// ),
	);
