<?php
use Rdl\Request\Request;

class BasketController extends Rdl\Loader\ControllerFactory{
	public function Options(){}
	
	public function IndexAction(){
		$this->Registry->SpeedBar[$this->Registry->Route->RealPage] = 'Корзина';
		switch($this->Registry->Route->Params['step']){
			// case 'verify':
			// 	if($this->Registry->Auth->isLogin()){
			// 		header('Location: '.str_replace('/verify/', '/stepOne/', $_SERVER['REQUEST_URI']));
			// 		die();
			// 	}
			// 	$this->Registry->SpeedBar['/basket/verify/'] = 'Авторизация';
			// 	$this->Verify();
			// 	break;
			case 'stepOne':
				$this->checkBasket();				
				$this->reloadToVerify();

				$this->Registry->SpeedBar['/stepOne/'] = 'Оформление заказа';
				$this->StepOne();
				break;
			case 'stepTwo':
				$this->checkBasket();				
				$this->reloadToVerify();

				$this->Registry->SpeedBar[] = 'Спасибо за заказ';
				$this->StepTwo();
				break;
			default:
				$this->Basket();
		}
	}

	public function reloadToVerify(){
		return false;
		if($this->Registry->Auth->isLogin() && true) return false;

		header('Location: /basket/verify/');
		return;
	}

	public function checkBasket(){
		if($this->Registry->SessionStorage->basket) return false;

		header('Location: /basket/');
		return;
	}

	public function Verify(){
		echo $this->ControllerTemplate->Display('index-verify');
		$this->ControllerTemplate->Clear();
	}

	public function StepOne(){
		echo $this->ControllerTemplate->Display('index-stepOne');
		$this->ControllerTemplate->Clear();
	}

	public function StepTwo(){
		$date = new \DateTime();
		$this->ControllerTemplate->Set('Payer', Request::Post('fullname'));
		$this->ControllerTemplate->Set('Adress', Request::Post('adress'));
		$this->ControllerTemplate->Set('City', Request::Post('city'));
		$this->ControllerTemplate->Set('Mobile', Request::Post('mobile'));
		$this->ControllerTemplate->Set('Email', Request::Post('email'));
		$this->ControllerTemplate->Set('arItems', $this->Registry->SessionStorage->basket['items']);
		$this->ControllerTemplate->Set('arItemsData', $this->thisModel()->GetItems());
		$this->ControllerTemplate->Set('deliveryDate', $date->modify('+1 day')->format('d.m.Y'));
		$this->ControllerTemplate->Set('Number', Rdl\Components\Catalog\Basket::CheckNumber((int) $this->thisModel()->SendBasket()));
		echo $this->ControllerTemplate->Display('index-stepTwo');
		$this->ControllerTemplate->Clear();
		unset($this->Registry->SessionStorage->basket);
	}

	
	public function Basket(){
		$this->ControllerTemplate->Set('arItems', $this->Registry->SessionStorage->basket['items']);
		$this->ControllerTemplate->Set('arItemsData', $this->thisModel()->GetItems());
		echo $this->ControllerTemplate->Display('index-1');

	}

	public function ShortInclude(){
		$this->UpdateAjaxAction();
	}

	public function UpdateAjaxAction(){
		if(isset($this->Registry->SessionStorage->basket)){
		 	$this->ControllerTemplate->Set('CountItem', (int) count($this->Registry->SessionStorage->basket['items']));
		 	$this->ControllerTemplate->Set('Money', $this->Registry->SessionStorage->basket['Money']);
		}

		if($this->Core->Service == 'ajax'){
			echo $this->ControllerTemplate->Display('short');	
			die();
		}
			
		$this->ControllerTemplate->Display('short');	
	}

	public function UpdateAllBasketAjaxAction(){
		if(empty($this->Registry->SessionStorage->basket['items']))
			die(json_encode(array('allprice' => '0', 'items' => array())));
		
		$arReturn = array();
		foreach($this->Registry->SessionStorage->basket['items'] as $Key => $arItem){
			$arReturn[$Key] = number_format(($arItem['price'] * $arItem['count']));
		}

		die(json_encode(array('allprice' => number_format($this->Registry->SessionStorage->basket['Money']), 'items' => $arReturn)));
	}

	public function deleteitemAjaxAction(){
		if(!isset($this->Registry->SessionStorage->basket['items'][Request::Post('item')]))
			die('false');
		$this->Registry->SessionStorage->basket['Money'] -= ($this->Registry->SessionStorage->basket['items'][Request::Post('item')]['count'] * $this->Registry->SessionStorage->basket['items'][Request::Post('item')]['price']);
		unset($this->Registry->SessionStorage->basket['items'][Request::Post('item')]);

		die('true');
	}

	public function updateColAjaxAction(){
		if(!isset($this->Registry->SessionStorage->basket['items'][Request::Post('item')]))
			die('false');
		$this->Registry->SessionStorage->basket['Money'] -= (($this->Registry->SessionStorage->basket['items'][Request::Post('item')]['count'] - (int) Request::Post('count')) * $this->Registry->SessionStorage->basket['items'][Request::Post('item')]['price']);
		$this->Registry->SessionStorage->basket['items'][Request::Post('item')]['count'] = (int) Request::Post('count');
		die('true');
	}

	public function AddBasketAjaxAction(){
		if(!isset($this->Registry->SessionStorage->basket)){
			$this->Registry->SessionStorage->basket = array(
				'items' => array(
					Request::Post('size') .'_'. Request::Post('item_id') => array(
					'item_id' => (int) Request::Post('item_id'),
					'count'	=> (int) Request::Post('count'),
					'price' => Request::Post('price')
					)
				),
				'Money' => (int) (Request::Post('price') * Request::Post('count'))
				);
		} else {
			if(isset($this->Registry->SessionStorage->basket['items'][Request::Post('size') .'_'. Request::Post('item_id')]))
				$this->Registry->SessionStorage->basket['items'][Request::Post('size') .'_'. Request::Post('item_id')]['count'] += (int) Request::Post('count');
			else 
				$this->Registry->SessionStorage->basket['items'][Request::Post('size') .'_'. Request::Post('item_id')] = array('item_id' => (int) Request::Post('item_id'), 'count' => (int) Request::Post('count'), 'price' => Request::Post('price'));
			
			$this->Registry->SessionStorage->basket['Money']  += (Request::Post('price') * Request::Post('count'));
		}
	}
}
