<?php

use Rdl\Request\Request;
use Rdl\Cache\RedisCache;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;

class BaseModel extends \Rdl\Loader\ModelFactory {
	
	protected $Count = 0;
	
	protected $obRedis = false;

	public function Query(){
		$arQueryWhere = array();
		if(mb_strlen(trim(Request::Post('inp'))) > 3){
			if(strpos(Request::Post('inp'), ',') !== false){
				$arQuerys = explode(',', Request::Post('inp'));
				$arResultQuery = array();
				foreach($arQuerys as $key => $value){
					$value = trim($value);
					if(mb_strlen($value) <= 1) continue;
					
					$arResultQuery[] = $value;
				}
				unset($arQuerys);
				$arQueryWhere[] = '`AlphaCode` IN ("'.join('","', $arResultQuery).'")';
			} else {
				$arQueryWhere['AlphaCode'] = '`AlphaCode` = "'. Request::Post('inp') .'"';
			}
		}
		if(mb_strlen(trim(Request::Post('lastname'))) >= 2){
			$arQueryWhere['lastname'] = '`lastname` LIKE \'%'. Request::Post('lastname') .'%\'';
		}
		if(mb_strlen(trim(Request::Post('fullname'))) > 2) 
			$arQueryWhere[] = '`fullname` LIKE \'%'. Request::Post('fullname') .'%\'';
		
		if(mb_strlen(trim(Request::Post('dateTime'))) >= 4){
			if(mb_strlen(trim(Request::Post('dateTime'))) == 4){
				$arQueryWhere[] = '`dateByrsday` LIKE \'%'. (int) trim(Request::Post('dateTime')) .'%\'';			
			} else {
				$date = new DateTime(trim(Request::Post('dateTime')));
				$arQueryWhere[] = '`dateByrsday` = "'. $date->format('Y-m-d') .'"';
			}
		}

		if(count($arQueryWhere) <= 0){
			die();
			return false;
		}

		$Query = 'SELECT `id`, `fullname`, `lastname`, `otvname`, `dateByrsday`, `area`, `archive`, `pat` FROM `usersb` WHERE ' . join(' AND ', $arQueryWhere) .' ORDER BY lastname,fullname';
		
		$obQuery = $this->Registry->DataBase->Query($Query)->execute();
		$this->Count = $obQuery->Count();
		
		unset($arQueryWhere);
		
		$arData = array();
		while($arList = $obQuery->current()){
			if(Request::Post('lastname'))
				$arList['lastname'] = preg_replace('/('.Request::Post('lastname').')/iu','<strong>$1</strong>' , $arList['lastname']);
			
			$date = new DateTime($arList['dateByrsday']);
			$arList['dateByrsday'] = $date->format('d.m.Y');
			$arData[$arList['id']] = $arList;
		}

		return count($arData) <= 0 ? false : $arData;
	}

	public function Count(){
		return $this->Count;
	}

	public function GetDetail(){
		if(!Request::Post('ID'))
			return false;


		$obQuery = new TableGateway('usersb', $this->Registry->DataBase);
		$arDetail = $obQuery->select(array('id' => (int) Request::Post('ID')))->current();
		if(empty($arDetail['id']))
			return false;
		
		$arCatalog = array(
			'medic' => 'medic', 
			'learnLocation' => 'university',
		);

		foreach($arDetail as $Key => $Value){
			if(!empty($arCatalog[$Key])){
				if($Key == 'medic'){
					$medic = $this->GetDataCache($Key);
					foreach($medic as $name => $value){
						if($value == $Value){
							$arDetail[$Key] = $name;
						}
					}
				}else
					$arDetail[$Key] = $this->LoadCache($arCatalog[$Key], $Value);
					
				continue;
			}

			$arDetail[$Key] = mb_strlen($Value) <= 0 ? '-' : $Value;
		}

		return $arDetail;
	}

	public function GetStreet($StreetID){
		if(intval($StreetID) <= 0)
			return false;
		
		$arResult = $this->LoadCache('street', $StreetID);

		if(empty($arResult['name']))
			return false;

		return $arResult['name']; 
	}

	public function GetType($TypeID){
		if(intval($TypeID) <= 0)
			return false;

		$arResult = $this->LoadCache('category', $TypeID);
		if(empty($arResult['name']))
			return false;

		return $arResult['name']; 
	}

	public function SaveParams(){
		if(!Request::Post('user_id'))
			return false;


		$obTable = new TableGateway('usersb', $this->Registry->DataBase, new RowGatewayFeature('id'));
		$Results = $obTable->select(array('id' => (int) Request::Post('user_id')));
		
		$obRow 	 = $Results->current();
		
		if(!$obRow['id'])
			return false;

		$arFields = array(
			'pat', 
			'archive', 
			'query', 
			'lastname', 
			'fullname', 
			'otvname', 
			'house', 
			'courpse', 
			'flat', 
			'index', 
			'PhoneCode', 
			'Phone',
			'MobilePhone',
			'appearance',
			'location',
			'byrsdayLocation',
			'passportDate',
			'passportInfo',
			'passportNumber',
			'passportSerial',
			'dateByrsday',
			'parent',
			'info',
			'textArea',
			'street',
			'category',
			'university',
			'medic'
		);
		
		unset($_POST['user_id']);

		foreach($_POST as $key => $value){
			if(in_array($key, $arFields)){
				if(in_array($key, array('appearance', 'dateByrsday', 'passportDate'))){
					if(Request::Post($key)){
						$obDate = new DateTime(trim(Request::Post($key)));
						$_POST[$key] = $obDate->format('Y-m-d');
					} else {
						$_POST[$key] = '0000-00-00';
					}
				} 
				$obRow->$key = Request::Post($key) == 2 ? 0 : Request::Post($key);
			}
		}
		unset($_POST);

		return $obRow->save();
	}
	public function dataCache(){
		if(empty($this->obRedis))
			$this->obRedis = new RedisCache();

		$arCacheData = array('street', 'university', 'medic', 'category');
		$arResult = array();
		foreach($arCacheData as $Table){
			if($Table == 'university')
				$arResult['learnLocation'] = $this->GetDataCache($Table);
			else
				$arResult[$Table] = $this->GetDataCache($Table);

		}
		die(json_encode($arResult));
	}

	private function GetDataCache($Table){
			$cacheID = $Table . ':arData';
			
			if(!$this->obRedis->hasItem($cacheID))
				$this->LoadCache($Table, true);

			$arKeys = array();
			foreach(array_keys(json_decode($this->obRedis->getItem($cacheID), true)) as $key)
				$arKeys[] = $Table . ':' . $key;
	
			$arResult = array();
			foreach($this->obRedis->getItems($arKeys) as $key => $jsonArray){
				$arItem = json_decode($jsonArray, true);
				
				switch($Table){
					case 'medic':
						$arItem['name'] = $arItem['fullName'];
						break;
					case 'university':
						$arItem['name'] = $arItem['smallname'];
						break;
				}

				$arResult[$arItem['name']] = $arItem['id']; 
			}

			return $arResult;
	}

	private function LoadCache($table, $itemID){

		if(empty($this->obRedis))
			$this->obRedis = new RedisCache();

		$cacheID = $table .':'. (int) $itemID;
		$cacheTableID = $table .':arData';
		if(!$this->obRedis->hasItem($cacheID)) {
			$obQuery = $this->Registry->DataBase->Query('SELECT * FROM `'.$table.'`')->execute();
			$arReturn = array();
			$arCache  = array();
			while($arRow = $obQuery->current()){
				$arReturn[$table .':'. $arRow['id']] = json_encode($arRow);
				$arCache[$arRow['id']] = 1; 
			}
			$this->obRedis->setItems($arReturn);
			if(!$this->obRedis->hasItem($cacheTableID))
				$this->obRedis->setItems(array($cacheTableID => json_encode($arCache)));
			
			unset($arCache, $arReturn);
		}

		return $arResult = json_decode($this->obRedis->getItem($cacheID), true);
	}
}
