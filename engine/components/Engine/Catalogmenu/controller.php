<?php
use Rdl\Request\Request;

class CatalogmenuController extends Rdl\Components\ControllerFactory{
	public function Options(){}
	
	public function DefaultInclude(){
		 $this->ControllerTemplate->Set('arCatalog', $this->thisModel()->GetMenuList());
		 $this->ControllerTemplate->Display('index');
		 $this->ControllerTemplate->Clear();
	}
}
