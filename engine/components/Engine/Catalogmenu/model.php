<?php
use Zend\Cache\StorageFactory;
use Rdl\Request\Request;
use Rdl\Components\Loader;
use Rdl\Components\File;
use Rdl\Components\Catalog\Catalog;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Rdl\Exception\ErrorPage;

class CatalogmenuModel extends \Rdl\Components\ModelFactory {
	public function GetMenuList(){
		$obCatalog = new Catalog();
		$arReturn = array();
		$arCategoryList = $obCatalog->GetCategoryList();
		if(!$arCategoryList)
			return array();

		foreach($arCategoryList as $categoryID => $arCategory){
			if($arCategory['parent_category'] != 0){
				$arReturn['category'][$arCategory['parent_category']]['children'][$categoryID] = $arCategory;
				$arReturn['category'][$arCategory['parent_category']]['children'][$categoryID]['link'] = $this->Registry->Route->RealPage . $obCatalog->GetCategoryList($arCategory['parent_category'])['altname'] .'/'. $arCategory['altname'] . '/';
			}else{
				$arReturn['category'][$categoryID] = $arCategory;
				$arReturn['category'][$categoryID]['link'] = $this->Registry->Route->RealPage . $arCategory['altname'] . '/';
			}
			
		}

		ksort($arReturn['category']);
		unset($arCategoryList);
		return $arReturn;
	}
}