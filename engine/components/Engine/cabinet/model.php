<?php
use Zend\Cache\StorageFactory;
use Rdl\Request\Request;
use Rdl\Components\Loader;
use Rdl\Components\File;
use Rdl\Components\Catalog\Catalog;
use Rdl\Components\Catalog\xFields;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\Sql\Select;
use Rdl\Exception\ErrorPage;

class CatalogdetailModel extends \Rdl\Loader\ModelFactory {
	
	protected $obCatalog = false;
	protected $obxFields = false;

	public function __construct(){
		parent::__construct();

		$this->obCatalog = new Catalog;
		$this->obCatalog->CacheRun();
		$this->obxFields = new xFields;
		$this->obxFields->RunCacheFields();
	}

	public function GetOrders(){
		
	}
}