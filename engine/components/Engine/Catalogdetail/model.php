<?php
use Zend\Cache\StorageFactory;
use Rdl\Request\Request;
use Rdl\Components\Loader;
use Rdl\Components\File;
use Rdl\Components\Catalog\Catalog;
use Rdl\Components\Catalog\xFields;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\Sql\Select;
use Rdl\Exception\ErrorPage;

class CatalogdetailModel extends \Rdl\Loader\ModelFactory {
	
	protected $obCatalog = false;
	protected $obxFields = false;

	public function __construct(){
		parent::__construct();

		$this->obCatalog = new Catalog;
		$this->obCatalog->CacheRun();
		$this->obxFields = new xFields;
		$this->obxFields->RunCacheFields();
	}

	public function GetDetail(){
		if(!isset($this->arParams['ID']))
			return false;
		

		$obQuery = new TableGateway('rdl_catalog', $this->Registry->DataBase, new RowGatewayFeature('id'));
		$ID = &$this->arParams['ID'];
		$arData = $obQuery->select(function(Select $select) use($ID){
			$select->columns(array('name', 'detail_image', 'date', 'new', 'info', 'surprice', 'article' => 'product_id'));
			$select->join(array('pr' => 'rdl_catalog_price'), 'pr.product_id = rdl_catalog.id');
			$select->where('rdl_catalog.id = '. (int) $this->arParams['ID']);
		})->current()->toArray();

		$obFile = new File();
		if($arData['detail_image'] != '0'){
			$arData['detail_image'] = $obFile->getImages((int) $arData['detail_image'], '200x0');
			
		}

		$arCategory = $this->obCatalog->getCategoryName($this->Registry->Route->Params['category'], $this->Registry->Route->Params['parent_category']);
		$this->Registry->SpeedBar['/catalog/' . $this->obCatalog->getCategoryName($this->Registry->Route->Params['parent_category'])['altname'].'/'] = $this->obCatalog->getCategoryName($this->Registry->Route->Params['parent_category'])['name'];
		$this->Registry->SpeedBar['/catalog/' . $this->obCatalog->getCategoryName($this->Registry->Route->Params['parent_category'])['altname'] .'/'. $arCategory['altname'].'/'] = $arCategory['name'];
		$this->Registry->SpeedBar['/catalog/' . $this->obCatalog->getCategoryName($this->Registry->Route->Params['parent_category'])['altname'] .'/'. $arCategory['altname'].'/'. $arData['id'] .'/'] = $arData['name'];
				
		return $arData;
	}

	public function GetxFields(){
		return $this->obxFields->getFieldsProduct((int) $this->arParams['ID']);
	}
}