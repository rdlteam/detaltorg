<?php
use Rdl\Request\Request;

class CatalogdetailController extends Rdl\Loader\ControllerFactory{
	public function Options(){}

	public function IndexAction(){
		$this->ControllerTemplate->Set('arItem', $this->thisModel()->GetDetail());
		$this->ControllerTemplate->Set('arxFields', $this->thisModel()->GetxFields());

		$this->ControllerTemplate->Display('index');
	}
}
