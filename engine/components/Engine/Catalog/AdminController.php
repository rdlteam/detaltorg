<?php

use Rdl\Request\Request;

Class CatalogController extends Rdl\Components\ControllerFactory  {

	protected $arStatus = array(
	        				0 => 'Не обработан',
	        				1 => 'Подтвержден',
	        				2 => 'Получен',
	        				3 => 'Отменен клиентом',
	        				4 => 'Оплачен',
	        				5 => 'Отменен'
	        			);	

	public function Options(){
		$this->Registry->SpeedBar['/catalog/'] = 'Каталог';
		$this->arParams['Template'] = 'admin';
	}

	public function IndexAction(){
		header('Location: /admin/catalog/orders/');
		die();
	}

	public function OrdersAction(){
		if($this->RequestOrder())
			return false;

		$this->Registry->SpeedBar['/catalog/orders/'] = 'Заказы';
		if(Request::Get('order_id')){
			$this->ShowOrder();
			return false;
		}
		$this->ControllerTemplate->Set('arStatus', $this->arStatus);
		$this->ControllerTemplate->Set('arOrders', $this->thisModel()->getOrders());
		$this->ControllerTemplate->Display('index');
	}

	public function CategoryAction(){
		$this->CategoryAjaxAction();
		$this->Registry->SpeedBar['/catalog/category/'] = 'Категории';
		$this->ControllerTemplate->Set('arCategory', $this->thisModel()->generateTreeCategory());
		$this->ControllerTemplate->Display('category/index');
	}

	public function CategoryAjaxAction(){
		return $this->thisModel()->ActionCategory();
	}

	public function AddCategoryAction(){
		$this->Registry->SpeedBar['/catalog/category/'] = 'Категории';
		$this->Registry->SpeedBar['/catalog/category/add/'] = 'Создать';

		$this->ControllerTemplate->Set('arCategory', $this->thisModel()->generateTreeCategory());
		$this->ControllerTemplate->Display('category/edit');
	}

	public function EditCategoryAction(){
		if(!Request::Get('id'))
			return false;

		$this->Registry->SpeedBar['/catalog/category/'] = 'Категории';
		$this->Registry->SpeedBar['/catalog/category/add/'] = 'Редактировать';
		$this->ControllerTemplate->Set('arData', $this->thisModel()->GetCategory());
		$this->ControllerTemplate->Set('arCategory', $this->LoadModel('Engine:catalogmenu')->GetMenuList());
		$this->ControllerTemplate->Display('category/edit');
	}

	public function ItemsAction(){
		$this->Registry->SpeedBar['/catalog/items/'] = 'Товары';
		$this->ControllerTemplate->set('arItems', $this->thisModel()->GetItemsData());
		$this->ControllerTemplate->Display('items/index');
	}

	public function AddItemsAction(){
		$this->Registry->SpeedBar['/catalog/items/add/'] = 'Добавить товар';
		$this->ControllerTemplate->Display('items/form');
	}

	private function RequestOrder(){
		if(!Request::Post('action') || !Request::Post('order_id')) return false;
		switch(mb_strtolower(Request::Post('action'), $this->Core->Config->charset)){
			case 'update_order':
				$this->thisModel()->UpdateOrder();
				return true;
				break;
			default:
				return false;
		}
	}


	public function ShowOrder(){
		$this->Registry->SpeedBar['/catalog/orders/?order_id=' . Request::Get('order_id')] = 'Редактирование заказа #'. Rdl\Components\Catalog\Basket::CheckNumber(Request::Get('order_id'));
		$this->ControllerTemplate->Set('arStatus', $this->arStatus);
		$this->ControllerTemplate->Set('arData', $this->thisModel()->getOrder());
		$this->ControllerTemplate->Display('order');
	}
}