<?php
use Zend\Cache\StorageFactory,
	Rdl\Request\Request,
	Rdl\Components\Loader,
	Rdl\Components\File,
	Rdl\Components\Catalog\Catalog,
	Rdl\Components\Catalog\xFields,
	Zend\Db\TableGateway\TableGateway,
	Zend\Db\TableGateway\Feature\RowGatewayFeature,
	Zend\Db\Sql\Select,
	Rdl\Exception\ErrorPage,
	Zend\Mail\Message,
	Zend\Mail\Transport\Sendmail as SendmailTransport,
	Rdl\Components\ComponentLoader;

class CatalogModel extends Rdl\Components\ModelFactory{

	protected $obCatalog = false;
	protected $arStatus = array(
	        				0 => 'Не обработан',
	        				1 => 'Подтвержден',
	        				2 => 'Получен',
	        				3 => 'Отменен клиентом',
	        				4 => 'Оплачен',
	        				5 => 'Отменен'
	        			);	

	public $Count = 0;
	function __construct(){
		parent::__construct();
		$this->obCatalog = new Catalog;
		$this->obCatalog->CacheRun();
		$this->obxFields = new xFields;
		$this->obxFields->RunCacheFields();
		if(Request::Get('clear_cache') == 'Y')
			$this->obCatalog->CacheClear();
	}

	public function GetData(){
		$Query = array();
		$Query[] = 'SELECT `rdl_catalog`.*, `rdl_catalog`.id as pid, `rdl_catalog_price`.*
				  	FROM `rdl_catalog`
				  	LEFT JOIN `rdl_catalog_price` ON `rdl_catalog_price`.product_id = `rdl_catalog`.id';

		$where = array();
		$category_name = false;
		$error = false;
		if(isset($this->Registry->Route->Params['parent_category'])) {
			if(!empty($this->Registry->Route->Params['category'])){
				
				$category_name = $this->obCatalog->getCategoryName($this->Registry->Route->Params['category'], $this->Registry->Route->Params['parent_category'])['name'];
				$Content = $this->obCatalog->getCategoryName($this->Registry->Route->Params['category'], $this->Registry->Route->Params['parent_category'])['content'];

				$this->Registry->SpeedBar['/catalog/' . $this->obCatalog->getCategoryName($this->Registry->Route->Params['parent_category'])['altname'].'/'] = $this->obCatalog->getCategoryName($this->Registry->Route->Params['parent_category'])['name'];
				$this->Registry->SpeedBar['/catalog/' . $this->obCatalog->getCategoryName($this->Registry->Route->Params['parent_category'])['altname'] .'/'. $this->obCatalog->getCategoryName($this->Registry->Route->Params['category'], $this->Registry->Route->Params['parent_category'])['altname'].'/'] = $category_name;
				$where[] = '`rdl_catalog`.category = "' . (int) $this->obCatalog->getCategoryName($this->Registry->Route->Params['category'], $this->Registry->Route->Params['parent_category'])['id'] .'"';
			}else{
				$category_name = $this->Registry->SpeedBar['/catalog/'.$this->obCatalog->getCategoryName($this->Registry->Route->Params['parent_category'])['altname'].'/'] = $this->obCatalog->getCategoryName($this->Registry->Route->Params['parent_category'])['name'];
				$Content = $this->obCatalog->getCategoryName($this->Registry->Route->Params['parent_category'])['content'];

				if($this->obCatalog->getCategorys($this->Registry->Route->Params['parent_category']) === false)
					$error = true;
				else
					$where[] = '`rdl_catalog`.category IN ('. join(',', array_keys($this->obCatalog->getCategorys($this->Registry->Route->Params['parent_category']))).')';
			}

		} 
		if($error) {
			return array(
			'CategoryName' => $category_name, 
			'Content' => &$Content,
			'Count' => $this->Count,
			'arItems' => array());
		}

		if(Request::Get('count') == 'Y')
			$where[] = '`rdl_catalog_price`.count > "0"';

		if(count($where) > 0){
			$Query[] = 'WHERE '. join(' AND ', $where);
			unset($where);
		}

		switch((int) Request::Get('sort')){
			case 1:
				$Query[] = 'ORDER BY `rdl_catalog_price`.price DESC';
				break;
			case 2:
				$Query[] = 'ORDER BY `rdl_catalog_price`.price ASC';
				break;
			case 3:
				$Query[] = 'ORDER BY `rdl_catalog`.view DESC';
				break;
			default:
				$Query[] = 'ORDER BY `rdl_catalog`.date DESC';		
		}

		// var_dump(join(' ', $Query));
		$Query = $this->getQuery(join(' ', $Query), 50);
		$this->Registry->Helper->headTitle()->append($category_name);
		
		if($this->Count <= 0)
			@header('HTTP/1.0 404 Not Found');

		return array(
			'CategoryName' => &$category_name,
			'Content' => &$Content,
			'Count' => $this->Count,
			'arItems' => $Query);
	}

	public function GetInclude($count_item){
		if(intval($count_item) <= 0)
			$count_item = 4;

		$Query = 'SELECT `rdl_catalog`.*, `rdl_catalog`.id as pid, `rdl_catalog_price`.* 
				  FROM `rdl_catalog` 
				  LEFT JOIN `rdl_catalog_price` ON `rdl_catalog_price`.product_id = `rdl_catalog`.id
				  WHERE `rdl_catalog`.new = 1';

		return $this->getQuery($Query, $count_item);
	}

	public function GetIncludePopular($count_item){
		if(intval($count_item) <= 0)
			$count_item = 4;

		$Query = 'SELECT `rdl_catalog`.*, `rdl_catalog`.id as pid, `rdl_catalog_price`.* 
				  FROM `rdl_catalog` 
				  LEFT JOIN `rdl_catalog_price` ON `rdl_catalog_price`.product_id = `rdl_catalog`.id
				  WHERE `rdl_catalog`.buy_count > 0
				  ORDER BY `rdl_catalog`.buy_count DESC';

		return $this->getQuery($Query, $count_item);
	}

	private function getQuery($Query, $count_item = 4){
		
		$obQuery = $this->Registry->DataBase->Query($Query . ' LIMIT '. $count_item)->execute();
		$this->Count = $this->Registry->DataBase->Query($Query)->execute()->Count();
		
		$arData = array();
		$arFileID = array();

		while($row = $obQuery->Current()){
			if(!empty($row['detail_image']))
				$arFileID[$row['pid']] = $row['detail_image'];
			if(!empty($this->Registry->Route->Params['category']))
				$category = $this->obCatalog->getCategoryName($this->Registry->Route->Params['category'])['altname'];
			else 
				$category = $this->obCatalog->getCategoryId($row['category'])['altname'];
			
			$parent_category = '';
			
			if(isset($this->Registry->Route->Params['parent_category']))
				$parent_category = $this->Registry->Route->Params['parent_category'];
			else{
				$parent_category = $this->obCatalog->getCategoryId($this->obCatalog->getCategoryId($row['category'])['parent_category'])['altname'];
			}

			$parent_category = $this->obCatalog->getCategoryName($parent_category)['altname'] .'/';

			$page = $this->Registry->Route->RealPage == '/' ? '/catalog/' : $this->Registry->Route->RealPage;

			$row['link'] = $page . $parent_category . $category .'/'. $row['id'];
			$arData[$row['pid']] = $row;
		}

		if(count($arFileID) > 0){
			$obFile = new File();
			$arFiles = $obFile->getImages($arFileID, '200x0');
			foreach($arFileID as $key => $ID){
				$arData[$key]['detail_image'] = $arFiles[$ID];
			}
			unset($arFiles);
		}


		return $arData;
	}


	//admins
	public function getOrders(){
		$obData = new TableGateway('rdl_orders', $this->Registry->DataBase);
		$arReturn = array();
		foreach($obData->Select()->toArray() as $key => $arItem){
			$arReturn[$arItem['id']] = $arItem;
		}

		return $arReturn;
	}

	public function getOrder(){
		if(!Request::Get('order_id')) return false;

		$obQuery = new TableGateway('rdl_orders', $this->Registry->DataBase, new RowGatewayFeature('id'));
		$obData = $obQuery->select(function(Select $select) {
			$select->columns(array('status', 'deliveryDate', 'deliveryType', 'fullname', 'phone', 'street', 'email', 'comment', 'city'));
			$select->join(array('items' => 'rdl_order_items'), 'items.order_id = rdl_orders.id');
			$select->where('rdl_orders.id = '. (int) Request::Get('order_id'));
		});

		$arData = array();
		$arItemsID = array();
		foreach($obData->toArray() as $key => $row){

			$arData[] = $row;

			if(!isset($arItemsID[$row['item_id']])) 
				$arItemsID[$row['item_id']] = true;
		}

		$obItems = $this->Registry->DataBase->Query('SELECT `rdl_catalog`.*, `rdl_catalog`.id as pid, `rdl_catalog`.product_id as article, `rdl_catalog_price`.*
				  		 FROM `rdl_catalog`
				  		 LEFT JOIN `rdl_catalog_price` ON `rdl_catalog_price`.product_id = `rdl_catalog`.id
				  		 WHERE `rdl_catalog`.id IN ('. join(',', array_keys($arItemsID)).')')->execute();
		unset($arItemsID);
		$arItems = array();
		$arImages = array();
		while($row = $obItems->current()){
			$arImages[$row['id']] = $row['detail_image'];
			$arItems[$row['id']] = $row;
		}


		if(count($arImages) > 0){
			$obFile = new File();
			$obImagesReturn = $obFile->getImages($arImages, '50x0');
			foreach($obImagesReturn as $key => $ID){
					$arItems[$key]['detail_image'] = $ID;
			}

			unset($arImages);
		}

		return array(
				'order_id' => (int) Request::Get('order_id'),
				'order' => $arData,
				'items' => $arItems,
			);
	}

	public function UpdateOrder(){
		$obQuery = new TableGateway('rdl_orders', $this->Registry->DataBase, new RowGatewayFeature('id'));
		$arTable = $obQuery->select(array('id' => Request::Post('order_id')))->current();
		$arTable->status = Request::Post('status');
		$arTable->maneger_id = $this->Registry->Auth->id;
		$arTable->save();


		// $obMessage = new Message();
		// $obMessage->addFrom("localhost@localhost.ru")
		// 		->addTo($arTable->email)
  //       		->setSubject("Статус заказа был изменен")
  //       		->setBody("Статус заказа на сайте WOW Shop был изменен\n Номер вашего заказа: ". $arTable->id ."\n Новый статус заказа: ". $this->arStatus[Request::Post('status')]);

		// $obTransport = new SendmailTransport();
		// $obTransport->send($obMessage);
		throw new Rdl\Exception\AjaxException(200, $this->arStatus[Request::Post('status')]);
	}

	public function ActionCategory(){
		if((!Request::Post('name') || !Request::Post('altname')) && !Request::Get('DeleteCategory'))
			return false;

		$obCategory = new TableGateway('rdl_catalog_category', $this->Registry->DataBase, new RowGatewayFeature('id'));

		if(!Request::Post('category_id') && !Request::Get('DeleteCategory')){
			
			if($this->obCatalog->getCategoryName(Request::Post('altname')))
				die(json_encode(array('class' => 'warning', 'message' => 'Такое альтернативное название уже присутвует')));

			$arData = array(
					'name' => Request::Post('name'),
					'altname' => Request::Post('altname'),
					'parent_category' => Request::Post('parent_category')
				);
			if(!$obCategory->Insert($arData)){
				$arError = array('class' => 'error', 'message' => 'Ошибка при создании категории');
				if($this->Core->isAjax())
					die(json_encode($arError));


				$this->Registry->SessionStorage->Message = &$arError;
				Request::Header('Location: /admin/catalog/category/');
				return true;
			} 

			$this->obCatalog->CacheClear();
			$arSuccess = array('class' => 'success', 'message' => 'Категория успешно создана', 'category_id' => $obCategory->getLastInsertValue());
			if($this->Core->isAjax())
				die(json_encode($arSuccess));

			$this->Registry->SessionStorage->Message = &$arSuccess;
			Request::Header('Location: /admin/catalog/category/');
			return true;
		}
		if(Request::Get('DeleteCategory') == 'Y'){
			$arCategory = $obCategory->Select(array('id' => (int) Request::Get('category_id')))->current();
			$nameCategory = $arCategory->name;
			
			if(!$arCategory->delete())
				$this->Registry->SessionStorage->Message = array('class' => 'error', 'message' => 'Произошла ошибка при удалении категории');
			else {
				$this->Registry->SessionStorage->Message = array('class' => 'success', 'message' => 'Категория <strong>'. $nameCategory .'</strong> была успешно удалена');
				$this->obCatalog->CacheClear();
			}

			Request::Header('Location: /admin/catalog/category/');
			die();
		}

		$arCategory = $obCategory->Select(array('id' => (int) Request::Post('category_id')))->current();

		$arCategory->name = Request::Post('name');
		$arCategory->altname = Request::Post('altname');
		$arCategory->parent_category = Request::Post('parent_category');
		$arCategory->save();

		$this->obCatalog->CacheClear();
		$arSuccess = array('class' => 'success', 'message' => 'Изменения в категории сохранены');
		if($this->Core->isAjax())
			die(json_encode($arSuccess));

		$this->Registry->SessionStorage->Message = &$arSuccess;
		Request::Header('Location: /admin/catalog/category/edit/?id='. Request::Post('category_id'));
		return true;
	}

	public function GetCategory(){
		return $this->obCatalog->getCategoryId((int) Request::Get('id'));
	}

	public function GetItemsData(){
			$obTable = new TableGateway('rdl_catalog', $this->Registry->DataBase);
			$obResult = $obTable->Select();
			$arResult = array();
			$arImages = array();

			while($arData = $obResult->current()){
				$arData['category'] = $this->obCatalog->GetTreeCategory($arData['category'], '/');
				$arResult[$arData->id] = $arData;

				$arImages[$arData->id] = $arData->detail_image;
			}
			
			$obFile = new File();
			$arTImages = $obFile->getImages(array_unique(array_values($arImages)), '70x0');
			foreach(array_keys($arResult) as $ID){
				$arResult[$ID]['image'] = $arTImages[$arImages[$ID]];
			}
			unset($arImages);

			$obTablePrice = new TableGateway('rdl_catalog_price', $this->Registry->DataBase);
			$obPriceResult = $obTablePrice->Select(array('product_id' => array_keys($arResult)));
			while($arPrice = $obPriceResult->current())
				$arResult[$arPrice->product_id]['price'] = array('base' => $arPrice->price, 'old_price' => $arPrice->old_price);

			// var_dump($arResult);
			unset($obResult);
			return $arResult;
	}

	public function generateTreeCategory(){
		$obComponentLoad = new ComponentLoader;

		$arData = $obComponentLoad->LoadModel('Engine:catalogmenu')->GetMenuList();
		$arCategory = array();

		foreach($arData['category'] as $categoryID => $arCategoryData){
			$arCategory[$categoryID] = $arCategoryData;
			if(isset($arCategoryData['children'])) foreach($arCategoryData['children'] as $childID => $arChildData){
				$arCategory[$childID] = $arChildData;
			}
			unset($arCategory[$categoryID]['children']);
		}

		return $arCategory;
	}
}