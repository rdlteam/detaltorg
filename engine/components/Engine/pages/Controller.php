<?php

class PagesController extends Rdl\Loader\ControllerFactory{
    public function Options(){
        $this->controllerPath = dirname(__FILE__);
        $this->ControllerName = 'pages';

        $this->ReloadControllerPath();
    }
    protected function IndexAction(){}
    public function defaultInclude(){
        
        if(empty($this->arParams['page_prefix']))
            $this->arParams['page_prefix'] = 'page';

        $paginator = new Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\Null((int) $this->arParams['count_elements']));
        $paginator->setCurrentPageNumber((int) Request::Get($this->arParams['page_prefix'], true, 1));
        $paginator->setItemCountPerPage($this->arParams['limit']);
        
        $this->ControllerTemplate->Set('obPagination', $paginator);
        $this->ControllerTemplate->Display('index');
    }
}