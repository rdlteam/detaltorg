<?php

use Rdl\Request\Request;
use Zend\Serializer\Serializer;

class NewsModel extends \Rdl\Loader\ModelFactory {
	function Get(array $WHERE = array(), array $arNavParams = array(), array $arSelect = array()){
		
		$Cache = new Rdl\Cache\CacheStorage('news');
		$cache_id = sha1(var_export(array($WHERE, $arNavParams, $arSelect), true) . 'cache_news' . Request::Get('page'));

		if($ReturnedCache = $Cache->Get($cache_id))
			return Serializer::factory('PhpSerialize')->unserialize($ReturnedCache);

		$addQuery = array();
		$addSelect = array();
		if(count($WHERE) > 0){

			$arWhere = array();
			foreach($WHERE as $key => $value){
				if(empty($key))
					$arWhere[] = $value;
				else
					$arWhere[] = '`'. $key .'` = '. $this->Registry->DataBase->EscapeString($value);
			}

			$addQuery[] = 'WHERE '. join(' ', $arWhere);
		}
		$addQuery[] = 'ORDER BY date DESC ';
		if(isset($arNavParams['PageLimit']) && (!isset($arNavParams['Nav']) || !$arNavParams['Nav'])){
			$limit[] = (int) $arNavParams['PageLimit'];
			$addQuery[] = 'LIMIT '. join(', ', $limit);
		}

		// if()

		if(count($arSelect) <= 0)
			$addSelect[] = '*';
		else {

		}

		$arResult = array();
		if(isset($arNavParams['Nav']) && $arNavParams['Nav']){
			$this->Registry->Navigation = new \Rdl\Components\Navigation();
			$this->Registry->Navigation->colPage = $arNavParams['PageLimit'];
			$Query = $this->Registry->Navigation->Query("SELECT ".join(', ', $addSelect) ." FROM `pf_news`". join('', $addQuery));
		}else 
			$Query = $this->Registry->DataBase->Query("SELECT ".join(', ', $addSelect) ." FROM `pf_news`". join('', $addQuery));
		
		while($arNews = $this->Registry->DataBase->GetRow($Query)){
			switch((int) $this->Core->Config->Seo->News->Param){
				case 2:
					if(empty($arNews['url']))
						$arNews['fLink'] = false;
					else
						$arNews['fLink'] = $this->Registry->Route->RealPage . '?news_url='. $arNews['url'];
					break;
				case 3:
					if(!empty($this->Core->Config->Seo->News->Url)) {
						
						$Url = $this->Core->Config->Seo->News->Url;
						if(strpos($this->Core->Config->Seo->News->Url, ':realpage:') !== false){
							if(in_array($this->Registry->Route->RealPage, array('/', '/index.php')))
								$this->Registry->Route->RealPage = $this->Registry->Route->RealPage .'news/';
							$Url = str_replace(':realpage:', $this->Registry->Route->RealPage, $Url);
						}

						if(strpos($this->Core->Config->Seo->News->Url, ':news-url:') !== false){
							if(empty($arNews['url'])) 
								$Url = false;
							else
								$Url = str_replace(':news-url:', $arNews['url'], $Url);
						}

						if(strpos($this->Core->Config->Seo->News->Url, ':news-id:') !== false){
							$Url = str_replace(':news-id:', $arNews['id'], $Url);
						}

						$arNews['fLink'] = $Url;

					} else 
						$arNews['fLink']  = false;
						break;
				case 1:
				default:
					$arNews['fLink'] = $this->Registry->Route->RealPage . '?news_id='. $arNews['id'];
			}

			$arResult[] = $arNews;
		}

		$Cache->Set($cache_id, $arResult);
		return $arResult;
	}

	public function GetFull($NewsParams = false, $Logic = 'AND'){
		if(!$NewsParams || count($NewsParams) <= 0) return false;

		$arQuery = array();
		if(isset($NewsParams['news_url']))
			$arQuery[] = '`url` = '. $this->Registry->DataBase->EscapeString($NewsParams['news_url']);
		if(isset($NewsParams['news_id']))
			$arQuery[] = '`id` = '. $this->Registry->DataBase->EscapeString($NewsParams['news_id']);

		$this->Registry->DataBase->Query('SELECT * FROM `pf_news` WHERE '. join($Logic, $arQuery) .' LIMIT 1');
		return $this->Registry->DataBase->GetRow();
	}
}