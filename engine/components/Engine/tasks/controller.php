<?php
use \Rdl\View\Template;
use \Rdl\helpers\helper;
use \Rdl\Request\Request;

Class TasksController extends Rdl\Loader\ControllerFactory  {

	protected $ControllerTemplate = false;

	public function Options(){
		$this->controllerPath = __DIR__;
		$this->ControllerName = 'tasks';
		if(!$this->ControllerTemplate)
			$this->ControllerTemplate = new Template();

		$path = ENGINE_DIR .'templates'. DIRECTORY_SEPARATOR .$this->Core->Config->Site->Template. DIRECTORY_SEPARATOR .'components'. DIRECTORY_SEPARATOR .$this->TypeComponent. DIRECTORY_SEPARATOR .'panel'. DIRECTORY_SEPARATOR;
		$this->ControllerTemplate->_path = is_dir($path) ? $path : $this->controllerPath . DIRECTORY_SEPARATOR .'templates'. DIRECTORY_SEPARATOR . (!$this->cTemplate ? 'default' : '') . DIRECTORY_SEPARATOR;
		$this->ControllerTemplate->Template = $this->Core->Url .'/engine/components/'. $this->TypeComponent .'/'. $this->ControllerName .'/templates/'. (!$this->cTemplate ? 'default' : '') .'/';
	}

	public function IndexAction(){
		if(!$this->Registry->Session->User['id']) 
			header('Location: /auth/');
		
		$Task = $this->thisModel()->GetTaskUser();

		$this->ControllerTemplate->Set('Task', $Task['Task']);
		$this->ControllerTemplate->Set('WorkTask', $Task['Work']);
		$this->ControllerTemplate->Display();
	}

	public function DetailAction(){

		if(!$arWork = $this->thisModel()->GetWork((int) $this->Registry->Route->Params['id']))
			throw new \Rdl\Exception\ErrorPage('404');
		
		helper::newJS($this->ControllerTemplate->Template . 'js/replay.js');

		$this->Registry->Template->title[] = $arWork['Work']['title'];
		$this->Registry->SpeedBar[] = $arWork['Work']['title'];

		$this->ControllerTemplate->Set('arWork', $arWork['Work']);
		$this->ControllerTemplate->Set('arWorkComments', isset($arWork['WorkComments']) ? $arWork['WorkComments'] : array());
		$this->ControllerTemplate->Set('Login', $this->Registry->Auth->Get('login'));
		$this->ControllerTemplate->Set('Users', $this->thisModel()->GetUsers());
		$this->ControllerTemplate->Display('detail');
	}

	// AJAX методы
	public function AddReplayAjaxAction(){
		if(!$this->Registry->Auth->GetParam('add_replay'))
			throw new \Rdl\Exception\AjaxException('110011', 'Вам запрещено добавлять ответы');

		if(!$arParams = Request::Post(array('work_id', 'message'), array('work_id' => 'int', 'message' => true))){
			throw new \Rdl\Exception\AjaxException('00121', 'Ошибка, не все параметры были переданы');
		}
		
		if(mb_strlen($arParams['message']) <= 2)
			throw new \Rdl\Exception\AjaxException('00122', 'Ошибка, введите сообщение не менее 2х символов');
		if(Request::Post('replay_user', 'int') && Request::Post('replay_user', 'int') <= 0)
			throw new \Rdl\Exception\AjaxException('00123', 'Ошибка, выберете ответсвенного');

		$this->Registry->DataBase->Query("SELECT `pf_task_work`.id, `upf_users`.email, `upf_users`.login
										  FROM `pf_task_work` 
										  LEFT JOIN `upf_users` ON `upf_users`.id = `pf_task_work`.user_id
										  WHERE `pf_task_work`.id = ". $this->Registry->DataBase->EscapeString($arParams['work_id'])." LIMIT 1");
		if($this->Registry->DataBase->GetNumRows() <= 0)
			throw new \Rdl\Exception\AjaxException('00124', 'Задания с таким id не найдено');

		$arWork = $this->Registry->DataBase->GetRow();
		$arFilds = array(
			'work_id' => $arWork['id'],
			'user_id' => $this->Registry->Auth->Get('id'),
			'user'	  => $this->Registry->Auth->Get('login'),
			'type'	  => (Request::Post('hide_message') == 'Y' && $this->Registry->Auth->GetParam('add_hide') ? 'hide' : 'open'),
			'text' 	  => nl2br($arParams['message']),
			'date'	  => time()
			);


		$addReplay = $this->Registry->DataBase->Query("INSERT INTO `pf_task_comments`(`work_id`, `user_id`, `user`, `type`, `text`, `date`) VALUES('".join("','", array_values($arFilds))."')");
		if(!$addReplay)
			throw new \Rdl\Exception\AjaxException('00125', 'Ошибка при добавлении в базу');
		
		if($arFilds['type'] == 'hide'){
			$message = "Уважаемый, ". $arWork['login'] .".\r\n В вашем обращении #".$arWork['id']." добавлен новый коментарий \r\n --------------- \r\n". $arParams['message'] ."\r\n";
			mail($arWork['email'], '[support] Ответ в обращении #'.$arWork['id'], $message);
		}

		mail('rdlrobot@gmail.com', '[support] Ответ в обращении #'.$arWork['id'], 'В обращении был получен ответ');
		$arFilds['date'] = date('d M Y H:i', $arFilds['date']);
		die(json_encode($arFilds));
	}
}