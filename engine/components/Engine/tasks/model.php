<?php
class TasksModel extends \Rdl\Loader\ModelFactory {
	public function GetTaskUser(){
		$return = array();
		$return['Task'] = $this->Registry->DataBase->GetRow($this->Registry->DataBase->Query('SELECT * FROM `pf_task_users` WHERE `user_id` = '. (int) $this->Registry->Auth->Get('id')));
		$return['Work'] = $this->GetTaskWork($return['Task']['id'], $return['Task']['user_id']);
		return $return;
	}

	public function GetTaskWork($task_id = false, $user_id = false){
		if(!$task_id) return false;
		$Where = array();
		if($task_id > 0){
			$Where[] = '`pf_task`.task_id = '.(int) $task_id;
		}
		if($user_id > 0){
			$Where[] = '`pf_task`.user_id = '.(int) $user_id;
		}

		$Where[] = "`pf_task_work`.title != 'NULL'";
		$this->Registry->DataBase->Query("SELECT 
										  `pf_task_work`.*,
										  `pf_task_project`.title as ProjectName,
										  `pf_task_project`.low_title,
										  `pf_task`.work_id,
										  `pf_task`.plan_hour,
										  `pf_task`.num_hour
										  FROM `pf_task` 
																  LEFT JOIN `pf_task_work` ON `pf_task_work`.id = `pf_task`.work_id 
																  LEFT JOIN `pf_task_project` ON `pf_task_project`.id = `pf_task_work`.project_id 
										  WHERE". join(' AND ', $Where));
		if($this->Registry->DataBase->GetNumRows() <= 0) return false;

		$return = array('AllHourse' => 0, 'TakeHourse' => 0);
		while($Task = $this->Registry->DataBase->GetRow()){
			$return['element'][$Task['work_id']] = $Task; 
			$return['AllHourse'] = $return['AllHourse'] + $Task['plan_hour'];
		}
		return $return;
	}

	public function GetWork($id = 0){
		if($id == 0) return false;
		$arWork = array();
		
		$arWork['Work'] = $this->Registry->DataBase->GetRow(
			$this->Registry->DataBase->Query("SELECT `pf_task_work`.*, `pf_users`.id as uid, `pf_users`.login FROM `pf_task_work` LEFT JOIN `pf_users` ON `pf_users`.id = `pf_task_work`.user_id WHERE `pf_task_work`.id = ". $id)
		);

		if(!$arWork['Work']) return false;

		$arWorkQuery = $this->Registry->DataBase->Query("SELECT `pf_task_comments`.*, `pf_users`.id as uid FROM `pf_task_comments` 
																  LEFT JOIN `pf_users` ON `pf_users`.id = `pf_task_comments`.user_id 
														 WHERE `pf_task_comments`.work_id = ". (int) $arWork['Work']['id'] . (!$this->Registry->Auth->GetParam('see_hide') ? ' AND `pf_task_comments`.type="open"':''));
		if($this->Registry->DataBase->GetNumRows($arWorkQuery) > 0) while($arWorkComment = $this->Registry->DataBase->GetRow($arWorkQuery)){
			$arWork['WorkComments'][$arWorkComment['id']] = $arWorkComment;
		}

		return $arWork;
	}

	public function GetUsers(){
		$arReturn = array();
		$arReturn['group-1'] = 'Производство';
		$arReturn['group-2'] = 'Тех поддержка';
		$arReturn['group-3'] = 'Разработка';

		$this->Registry->DataBase->Query("SELECT * FROM `upf_users_group` LEFT JOIN `upf_users` ON `upf_users`.id = `upf_users_group`.user_id WHERE `upf_users_group`.group_id IN (1,2)");
		if($this->Registry->DataBase->GetNumRows() > 0){
			while($arGroup = $this->Registry->DataBase->GetRow()){
				$arReturn['user-'.$arGroup['id']] = '['.$arReturn['group-'.$arGroup['group_id']].'] '.$arGroup['login'];
			}
		}
		return $arReturn;
	}
}