<?php
use \Rdl\Cache\CacheStorage;
Class BannersController extends Rdl\Loader\ControllerFactory  {
	
	public function Options(){
		$this->controllerPath = dirname(__FILE__);
		$this->ControllerName = 'banners';
	}

	public function defaultInclude(){
		if(isset($this->arParams['Limit']) && intval($this->arParams['Limit']) <= 0)
			return false;

		$this->Registry->Template->Set('arBanner', $this->thisModel()->Get($this->arParams['Type'], $this->arParams['Limit']));
		$this->Registry->Template->Display('banner-list-' . $this->arParams['Template']);
		$this->Registry->Template->Clear();
	}

}