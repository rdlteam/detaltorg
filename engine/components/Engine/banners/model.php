<?php

use Rdl\Request\Request;
use Zend\Serializer\Serializer;
use Rdl\Cache\CacheStorage;

class BannersModel extends \Rdl\Loader\ModelFactory {
	function Get($type = false, $col = 0){
		if($type === false) 
			return false;

		// $Cache = new CacheStorage('banners');
		// $CacheID = CacheStorage::GenerateCache('banners', array($type, $col));

		// if($ReturnedCache = $Cache->Get($CacheID)){

		// 	return Serializer::factory('PhpSerialize')->unserialize($ReturnedCache);
		// }

		$QuerySelect = "SELECT `id`, `name`, `link`, `img`, `position` FROM `rdl_banners` 
		WHERE (must_views > views OR 
				(start_view >= NOW() AND (end_view != '0000-00-00 00:00:00' AND end_view <= NOW()))) AND type = '" . $type ."'";
		if($col > 1)
			$QuerySelect .= ' ORDER BY `position`, `img`';

		$Query = $this->Registry->DataBase->Query($QuerySelect);
		$arBanners = array();
		while($arBanner = $this->Registry->DataBase->GetRow($Query)){
			$arBanners[] = $arBanner;
		}

		if(count($arBanners) <= 0)
			return false;

		if($col > 1){
			$Return = array();
			for($i = 0; $i < $col; $i++){
				if(!empty($arBanners[$i])){
					$Return[] = $arBanners[$i];
					$this->Registry->DataBase->Query('UPDATE `rdl_banners` SET `views` = (`views` + 1) WHERE `id` = '. (int) $arBanners[$i]['id']);
				} else $Return[] = 'null';
			}
			
		} else {
			$rand = mt_rand(0, (count($arBanners) - 1));
			shuffle($arBanners);
			$Return = $arBanners[$rand];
			$this->Registry->DataBase->Query('UPDATE `rdl_banners` SET `views` = (`views` + 1) WHERE `id` = '. (int) $Return['id']);
		}
		
		return $Return;
	}
}