<?php
use Zend\Cache\StorageFactory;
use Rdl\Request\Request;
use Rdl\Components\Loader;
use Rdl\Components\Menu;


class MenuModel extends \Rdl\Components\ModelFactory{

	public $obMenu = false;

	public function GetMenu(){
		$this->obMenu = new Menu;
		$this->CacheID = 'menu_' . $this->arParams['menu'];  
		$this->obMenu->CacheRun();
		return $this->obMenu->GetMenu($this->arParams['menu']);
	}
	
}