<?php
use Rdl\Request\Request;

class MenuController extends Rdl\Components\ControllerFactory{
	
	public function Options(){}
	
	public function DefaultInclude(){
		 $this->ControllerTemplate->Set('arMenu', $this->thisModel()->GetMenu());
		 $this->ControllerTemplate->Display('index');
		 $this->ControllerTemplate->Clear();
	}
}
