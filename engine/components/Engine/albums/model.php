<?php

use Rdl\Request\Request;
use Zend\Serializer\Serializer;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\Sql\Select;
use Bazalt\Thumbs\Image;

class AlbumsModel extends \Rdl\Loader\ModelFactory {
	public function __construct(){
		parent::__construct();
		Image::initStorage(ROOT_DIR . '/upload/image_cache', $this->Core->Url . '/index.php?thumb_file=/upload/image_cache');
	}
	public function GetAlbums(){
		$obQuery = $this->Registry->DataBase
			->Query('SELECT * FROM `rdl_albums`')
			->execute();
			
		$arLists = array();
		while($arList = $obQuery->current()){
			if($arList['logo'] != 0 && is_numeric($arList['logo']))
				$arList['logo'] = $this->GetImage((int) $arList['logo']);
			else if($arList['logo'] != 0)
				$arList['logo'] = filter_var($arList['logo'], FILTER_SANITIZE_STRING);
			
			$arLists[$arList['id']] = $arList;
		}

		$obQuery = $this->Registry->DataBase
			->Query('SELECT COUNT(*) as count, album_id FROM `rdl_albums_images` WHERE `album_id` IN ('.join(', ', array_keys($arLists)).')')
			->execute();
		while($arCount = $obQuery->current()){
			$arLists[$arCount['album_id']]['count'] = $arCount['count'];
		}

		return $arLists;
	}

	public function GetAlbumsImages(){
		if(!$this->Registry->Route->Params['album_id'])
			return array();

		$obAlbumQuery = new TableGateway('rdl_albums', $this->Registry->DataBase, new RowGatewayFeature('id'));
		$arAlbum = $obAlbumQuery->select(array('id' => (int) $this->Registry->Route->Params['album_id']))->current();
		
		if(empty($arAlbum->id))
			throw new Rdl\Exception\ErrorPage(404);

		$obQuery = new TableGateway('rdl_albums_images', $this->Registry->DataBase);
		$arAlbum['count_elements'] = $obQuery->Select(array('album_id' => (int) $arAlbum->id))->count();
		$arAlbum['limit_page'] = 84;
		$arAlbum['page_prefix'] = 'page';

		$arImage = $obQuery->Select(function (Select $select) use($arAlbum) {
									    $select->where(array('album_id' => (int) $arAlbum->id))
									     		->order('id ASC')
									    	    ->limit((int) $arAlbum['limit_page']);
									   	if(Request::Get($arAlbum['page_prefix']) > 1)
									   		$select->offset((int) $arAlbum['limit_page'] * (int) Request::Get($arAlbum['page_prefix'], true, 1));
									});

		$arLists = array('album' => $arAlbum, 'images' => array());
		while($arList = $arImage->current()){
			$arList['thumbs_link'] = Image::getThumb(ROOT_DIR . '/upload/albums/' . $arList['full_link'], '150x0', []);
			$arLists['images'][] = $arList;
		}

		return $arLists;
	}

	public function GetImage($imageID){
		if((int) $imageID <= 0)
			return '0';

		$obQuery = new TableGateway('rdl_albums_images', $this->Registry->DataBase);
		$arImage = $obQuery->select(array('id' => (int) $imageID))->current();
		if(empty($arImage['id']))
			return '0';

		return Image::getThumb(ROOT_DIR . '/upload/albums/' . $arImage['full_link'], '290x0', []);
	}
}