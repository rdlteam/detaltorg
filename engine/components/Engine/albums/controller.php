<?php
use \Rdl\Cache\CacheStorage;
Class AlbumsController extends Rdl\Loader\ControllerFactory  {
	
	public function Options(){
		$this->controllerPath = dirname(__FILE__);
		$this->ControllerName = 'albums';
		$this->SpeedBar[] = 'Альбомы';

		$this->ReloadControllerPath();
	}

	public function IndexAction(){
		if(!empty($this->Registry->Route->Params['album_id'])){
			$this->DetailAlbumsAction();
		} else {
			$this->ControllerTemplate->Set('arAlbums', $this->thisModel()->GetAlbums());
			$this->ControllerTemplate->Display('index');
		}
	}

	public function DetailAlbumsAction(){
		$arImages = $this->thisModel()->GetAlbumsImages();
		$this->ControllerTemplate->Set('arAlbums', $arImages['album']);
		$this->ControllerTemplate->Set('arImages', $arImages['images']);
		$this->ControllerTemplate->Display('albums');
	}
}