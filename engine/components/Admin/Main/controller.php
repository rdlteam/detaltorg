<?php

use \Rdl\View\Template,
	\Rdl\Request\Request;

class MainController extends Rdl\Components\ControllerFactory {
	
	public function Options(){}

	public function IndexAction(){
		$this->ControllerTemplate->Set('arData', $this->thisModel()->ReturnArData());
		$this->ControllerTemplate->Display('index');
	}
 	
 	public function UpdateAjaxAction(){
 		die($this->thisModel()->ReturnArData());
 	}
}
