<?php

use Rdl\Request\Request;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;

class MainModel extends \Rdl\Components\ModelFactory {
	
	public $cacheSize = 0;

	public function ReturnArData(){
		$arReturn = array(
			'cache_size' => $this->format_size($this->get_dir_size(ENGINE_DIR .'cache/')),
			'mysql_size' => $this->format_size($this->getMysqlSize()),
			'count_users' => $this->getCount('rdl_users')
			);

		if($this->Core->isAjax())
			return Zend\Json\Json::encode($arReturn);

		return $arReturn;
	}

	private function getCount($table = ''){
		if(empty($table))
			return 0;

		$obQuery = $this->Registry->DataBase->Query('SELECT COUNT(*) as count FROM `'. $table .'`')->execute();
		$arData = $obQuery->current();

		return $arData['count'];
	}

	private function getMysqlSize(){
		$result = $this->Registry->DataBase->Query('SHOW TABLE STATUS FROM `'. $this->Core->Config->db->database .'`')->execute();
		$mysql_size = 0;
		while ( $r = $result->current()) 
			$mysql_size += $r['Data_length'] + $r['Index_length'];
		
		return $mysql_size;
	}
	private function get_dir_size($directory) {
		$size = 0;
		
		foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory)) as $file) {
			$size += $file->getSize();
		}
		
		return $size;
	}

	private function format_size($size) {
  		$mod = 1024;
  		$units = explode(' ','B KB MB GB TB PB');
  		for ($i = 0; $size > $mod; $i++) {
  		  $size /= $mod;
  		}
		
  		return round($size, 2) . ' ' . $units[$i];
	}
}
