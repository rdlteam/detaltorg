<?php

use Rdl\Request\Request;
use Rdl\Components\Loader;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Rdl\Exception\ErrorPage;

class PagesModel extends \Rdl\Components\ModelFactory {

	public function getPages(){

		$obQuery = new TableGateway('rdl_pages', $this->Registry->DataBase);
		
		$arPagesStack = array();
		$arPagesQuery = array();
		
		$StartPageHash = false;

		foreach($obQuery->select(array('service' => 'index'))->toArray() as $arPage){

			$arPagesStack[$arPage['id']] = $arPage;
			
			if($arPage['parent'] > 0 && !empty($arPagesStack[$arPage['parent']])){
				$arPage['url'] = $arPagesStack[$arPage['parent']]['url'] . $arPage['url'];
			}
			
			$arPagesQuery[sha1(trim($arPage['url']))] = $arPage;
		}
		unset($arPagesStack);

		return $arPagesQuery;
	}

	public function GetPage($id){
		if($id <= 0)
			throw new ErrorPage(404);

		$obQuery = new TableGateway('rdl_pages', $this->Registry->DataBase);
		$arResult = $obQuery->select(array('id' => $id))->current();
		if(empty($arResult))
			throw new ErrorPage(404);

		return $arResult;
	}

	public function SavePage(){
		if(!Request::GetParam(array('method' => 'POST', 'var' => 'id')))
			return false;

		Rdl\Security\Security::getInstance()->verifyOrFail();
		$obTable = new TableGateway('rdl_pages', $this->Registry->DataBase, new RowGatewayFeature('id'));
		$Results = $obTable->select(array('id' => (int) Request::Post('id')));
		$obRow 	 = $Results->current();
		
		if(!$obRow['id'])
			throw new ErrorPage(301);

		foreach($this->PostData(array('title', 'url', 'keyword', 'desc', 'page', 'template', 'controller', 'action')) as $key => $value){
			$obRow->$key = $value;
		}

		Loader::getInstance()->ClearCache('pages-index');
		if(!$obRow->save())
			header('Location:/admin/pages/');
			

		header('Location:/admin/pages/edit/?id=' . (int) $obRow->id);
		die();
	}

	public function AddPage(){
		if(!Request::GetParam(array('method' => 'POST', 'var' => 'id')))
			return false;
		Rdl\Security\Security::getInstance()->verifyOrFail();

		$arInsert = $this->PostData(array(
				'title',
				'url',
				'keyword',
				'desc',
				'page',
				'template',
				'controller',
				'action'
			));
		

		$arInsert['service'] = 'index';
		$arInsert['parent'] = 1;
		$arInsert['lang'] = 'ru';
		// $arInsert['modify_date'] = new DateTime('NOW');
		$arInsert['modify_date'] = time();

		$this->Registry->DataBase->Query('INSERT INTO `rdl_pages`(`'. join("`, `", array_keys($arInsert)) .'`) VALUES("'. join('","', array_values($arInsert)).'")')->execute();
			
		Loader::getInstance()->ClearCache('pages-index');
		Request::Header('Location:/admin/pages/');
		die();
	}

	private function PostData(array $arFields = array()){
		$arInsert = array();

		foreach($_POST as $key => $value){
			if(!in_array($key, $arFields))
				continue;
			
			if($key == 'url' && strpos(trim($value), '.php') === false && strpos(trim($value), '.html') === false)
				$value = rtrim($value, '/') .'/';
			
			
			$arInsert[$key] = $value;
		}
		unset($_POST);
		
		return $arInsert;
	}

	public function DeletePage(){
		if(!Request::GetParam(array('method' => 'GET', 'var' => 'id')))
			return false;

		$obTable = new TableGateway('rdl_pages', $this->Registry->DataBase, new RowGatewayFeature('id'));
		$Results = $obTable->select(array('id' => (int) Request::GetParam(array('method' => 'GET', 'var' => 'id'))));
		$obRow 	 = $Results->current();

		if(empty($obRow))
			throw new Exception('Error');

		$obRow->Delete();

		Loader::getInstance()->ClearCache('pages-index');
		Request::Header('Location:/admin/pages/');
		die();
	}
}
