<?php
/**
* RDL CMS Pages Controller
* Контроллер управления страницами сайта
*
* @link      http://www.rdl-team.ru/ official site
* @copyright Copyright (c) 2013 RDL Team. (http://www.rdl-team.ru)
* @license   http://www.rdl-team.ru/license/new-bsd New BSD License
*/

class PagesController extends Rdl\Components\ControllerFactory {
	
	public function Options(){
		$this->Registry->SpeedBar['/pages/'] = 'Страницы сайта';
	}

	public function IndexAction(){
	
		// Шаблон, и параметры
		$this->ControllerTemplate->Set('arPages', $this->thisModel()->getPages());
		$this->ControllerTemplate->Display('index');
	}

	public function EditAction(){
		$this->Registry->SpeedBar['/pages/edit/'] = 'Редактирование страницы';
		// Инициализация метода сохранения страницы
		$this->thisModel()->SavePage();

		// Шаблон, и параметры
		$this->ControllerTemplate->Set('arData', $this->thisModel()->GetPage((int) $this->Registry->Route->Params['id']));
		$this->ControllerTemplate->Display('edit');
	}

	public function AddAction(){
		$this->Registry->SpeedBar['/pages/add/'] = 'Создание страницы';
		// Инициализация метода создания страницы
		$this->thisModel()->AddPage();

		// Шаблон, и параметры
		$this->ControllerTemplate->Set('arData', array(	'id' => 'add', 
														'url' => '',
														'title' => '', 
														'desc' => '', 
														'page' => '', 
														'controller' => '', 
														'action'	=> '',
														'template'	=> '',
														)
		);

		$this->ControllerTemplate->Display('edit');
	}

	public function DeleteAction(){
		$this->thisModel()->DeletePage();
	}

	/* Ajax Methods */
	public function DeleteAjaxAction(){
		return $this->DeleteAction();
	}
}
