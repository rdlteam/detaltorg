<?php
ob_start ();
ob_implicit_flush ( 0 );
ini_set('display_errors','on');
error_reporting(E_ALL);

define('SECURITY', true);
define('ROOT_DIR', __DIR__);
define('ENGINE_DIR', ROOT_DIR . '/engine/');
use Rdl\Loader\AutoLoader;
include_once ENGINE_DIR .'Core/lib/Rdl/Loader/AutoLoader.php';
$RDLClassLoader = new AutoLoader();	
$RDLClassLoader->setIncludePath(ENGINE_DIR. 'Core/lib');
$RDLClassLoader->register();

include_once(ENGINE_DIR .'Core/Core.php');

try {

	RDLCore::getInstance('index')->Run();
	echo "\n<!--Генерация: ". Rdl\Microtimer\Microtimer::getInstance()->get() ."-->\n";
	
} catch(\Exception $obException){
	if(!method_exists($obException, 'Display'))
		die($obException->getMessage());

	$obException->Display();
}